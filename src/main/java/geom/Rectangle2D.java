package geom;

import java.util.*;

/**
 * Describes a rectangle, with position at the bottom left point
 */
public class Rectangle2D {
	/**
	 * The bottom left point of the rectangle
	 */
	private Vector2D pos;
	ArrayList<Vector2D> points;
	ArrayList<Line2D> lines;
	private double width;
	private double height;

	public Rectangle2D(double posX, double posY, double width, double height) {
		setPos(new Vector2D(posX, posY));
		this.setWidth(width);
		this.setHeight(height);
		calcPoints();
		calcLines();
	}
	
	public Rectangle2D(Vector2D pos, double width, double height) {
		this.setPos(pos);
		this.setWidth(width);
		this.setHeight(height);
		calcPoints();
		calcLines();
	}

	public Rectangle2D(Vector2D pos, Vector2D size) {
		this.setPos(pos);
		this.setWidth(size.getX());
		this.setHeight(size.getY());
		calcPoints();
		calcLines();
	}
	
	void calcPoints() {

		points = new ArrayList<Vector2D>();

		points.add( pos.copy() );
		points.add( pos.add(0, height) );
		points.add( pos.add(width, height) );
		points.add( pos.add(width, 0) );
	}

	void calcLines() {
		lines = new ArrayList<Line2D>();
		for (int x = 0; x < 4; x++) {
			int next = (x < 3 ? x+1 : 0);
			lines.add(new Line2D(points.get(x), points.get(next) ) );
		}
	}
	
	public Vector2D move(Vector2D movt) { 
		setPos(getPos().add(movt));
		return getPos();
	}
	
	public void setPos(Vector2D newPos) {
		pos = newPos;
	}

	public boolean encloses(Vector2D point) {
		Vector2D diff = mid().sub(point);
		
		if ( Math.abs(diff.getX()) <= getWidth()/2 &&
			 Math.abs(diff.getY()) <= getHeight()/2 ) {
			return true;
		}
		
		return false;
	}
	
	public boolean overlaps(Rectangle2D other) {
		Vector2D diff = mid().sub( other.mid() );
		
		if ( Math.abs(diff.getX()) <= (getWidth() + other.width())/2 &&
			 Math.abs(diff.getY()) <= (getHeight() + other.height())/2 ) {
			return true;
		}
		
		return false;
	}

	public Rectangle2D add(Vector2D offset)	{
		return new Rectangle2D(pos.add(offset), width, height);
	}

	public Vector2D pos() { return getPos(); }
	public Vector2D mid() { return getPos().add((new Vector2D(getWidth(), getHeight())).div(2)); }
	public Vector2D opp() { return getPos().add( new Vector2D(getWidth(), getHeight())); }
	
	public double getX() { return pos().getX(); }
	public double getY() { return pos().getY(); }
	public int getXi() { return (int) getX(); }
	public int getYi() { return (int) getY(); }
	
	public double getOppX() { return opp().getX(); }
	public double getOppY() { return opp().getY(); }
	public int getOppXi() { return (int) getOppX(); }
	public int getOppYi() { return (int) getOppY(); }
	
	public Vector2D getPoint(int n) { return points.get(n); }
	public ArrayList<Vector2D> points() { return points; }
	public Line2D getLine(int n) { return lines.get(n); }
	public ArrayList<Line2D> lines() { return lines; }
	
	public double width() { return getWidth(); }
	public double height() { return getHeight(); }

	
	public Rectangle2D copy() {
		return new Rectangle2D(getPos(), getWidth(), getHeight());
	}

	public Vector2D getPos() {
		return pos;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}


	public Rectangle2D flipAboutX(Vector2D axis) {
		// The BLP of the new flipped rectangle will be the flipped BRP of the
		// original rectangle.  The width and height will remain the same.

		// Get the BRP of this rectangle
		Vector2D brp = this.pos.add(this.width, 0);

		// Flip to get new BLP
		Vector2D newBlp = brp.flipAboutX(axis);

		// Make and return new rect
		return new Rectangle2D(newBlp, this.width, this.height);
	}
}
