package controllers;

/* 
 * An Interface to abstract functionality of the input device, such that any input device
 * can be used
 */
public interface IController {
	/**
	 * Is true if the button has only just been pressed this frame
	 * @param action
	 * @return
	 */
	public boolean justPressed(Controls action);

	/**
	 * Is true only if the button has been held since a previous frame
	 * @param action
	 * @return
	 */
	public boolean held(Controls action);

	/**
	 * Is true if the button has either just been pressed or has been held.
	 * @param action
	 * @return
	 */
	public boolean active(Controls action);

	/**
	 * Is true if the button has just been released this frame.
	 * @param action
	 * @return
	 */
	public boolean justReleased(Controls action);

	public void refresh();
//	public void remap(Controls newControl, Integer key);
//	public void consume(Controls pause);
}
