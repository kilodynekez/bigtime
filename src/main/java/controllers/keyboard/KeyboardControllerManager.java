package controllers.keyboard;

import controllers.ControlState;
import controllers.Controls;
import controllers.IController;
import org.lwjgl.glfw.GLFWKeyCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;


public class KeyboardControllerManager {

	List<BaseKeyboardController> keyboardControllers = new ArrayList<>();

	public KeyboardControllerManager(long window) {

		glfwSetKeyCallback(window, new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				for (BaseKeyboardController controller : keyboardControllers) {
					for (Controls control : controller.keyMap.getOrDefault(key, new ArrayList<>())) {
						switch(action){
							case GLFW_PRESS:    // The key was just pressed
								controller.statusMap.put(control, ControlState.JustPressed);
								break;
							case GLFW_REPEAT:   // The key has been held
								controller.statusMap.put(control, ControlState.Held);
								break;
							case GLFW_RELEASE:  // The key was just released
								controller.statusMap.put(control, ControlState.JustReleased);
								break;
						}
					}
				}
			}
		});
	}

	public void register(BaseKeyboardController controller) {
		this.keyboardControllers.add(controller);
	}

	public void refresh() {
		for (BaseKeyboardController controller : keyboardControllers) {
			controller.refresh();
		}
	}




//	public void remap(Controls newControl, Integer key) {
//		keyMap.put(newControl, key);
//	}
//
//	public void consume(Controls action) {
//		KeyboardHelper.consume( keyMap.get(action) );
//	}
}
