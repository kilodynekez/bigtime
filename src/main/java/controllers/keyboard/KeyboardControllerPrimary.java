package controllers.keyboard;


import controllers.Controls;

import static org.lwjgl.glfw.GLFW.*;

public class KeyboardControllerPrimary extends BaseKeyboardController {

    public KeyboardControllerPrimary(KeyboardControllerManager manager) {
        super(manager);
    }

    @Override
    protected void initKeys() {
        bindKey(GLFW_KEY_A, Controls.LEFT);
        bindKey(GLFW_KEY_D, Controls.RIGHT);
        bindKey(GLFW_KEY_W, Controls.UP);
        bindKey(GLFW_KEY_S, Controls.DOWN);

        bindKey(GLFW_KEY_M, Controls.ToggleMovt);
        bindKey(GLFW_KEY_Q, Controls.SHRINKSELF);


        bindKey(GLFW_KEY_N, Controls.ToggleDebugView);
//		keyMap.put(Controls.JUMP, Keyboard.KEY_J);
//		keyMap.put(Controls.SPRINT, Keyboard.KEY_SEMICOLON);
//		keyMap.put(Controls.CLIMB,  Keyboard.KEY_K);
//		keyMap.put(Controls.USE_ITEM, Keyboard.KEY_I);
//		keyMap.put(Controls.SELECT, Keyboard.KEY_RETURN);
//		keyMap.put(Controls.PAUSE, Keyboard.KEY_ESCAPE);
//		keyMap.put(Controls.DIG, Keyboard.KEY_L);
    }
}
