package controllers.keyboard;

import controllers.ControlState;
import controllers.Controls;
import controllers.IController;
import org.lwjgl.glfw.GLFWKeyCallback;

import java.util.HashMap;


import java.util.*;

import static org.lwjgl.glfw.GLFW.*;


public class BaseKeyboardController implements IController {
	HashMap<Integer, List<Controls>> keyMap;

	HashMap<Controls, ControlState> statusMap;

	static List<BaseKeyboardController> keyboardControllers = new ArrayList<>();

	protected void initKeys() {
		bindKey(GLFW_KEY_A, Controls.LEFT);
		bindKey(GLFW_KEY_D, Controls.RIGHT);
		bindKey(GLFW_KEY_W, Controls.UP);
		bindKey(GLFW_KEY_S, Controls.DOWN);
		bindKey(GLFW_KEY_M, Controls.ToggleMovt);
//		keyMap.put(Controls.JUMP, Keyboard.KEY_J);
//		keyMap.put(Controls.SPRINT, Keyboard.KEY_SEMICOLON);
//		keyMap.put(Controls.CLIMB,  Keyboard.KEY_K);
//		keyMap.put(Controls.USE_ITEM, Keyboard.KEY_I);
//		keyMap.put(Controls.SELECT, Keyboard.KEY_RETURN);
//		keyMap.put(Controls.PAUSE, Keyboard.KEY_ESCAPE);
//		keyMap.put(Controls.DIG, Keyboard.KEY_L);
	}
	
	public BaseKeyboardController(KeyboardControllerManager manager) {
		// Add this instance to the list of all keyboard controllers
		manager.register(this);


		keyMap = new HashMap<>();
		statusMap = new HashMap<>();
		
		initKeys();

		for (List<Controls> controls : keyMap.values()) {
			for (Controls control : controls) {
				statusMap.put(control, ControlState.Open);
			}
		}
	}



	protected void bindKey(Integer key, Controls control)	{
		if (!keyMap.containsKey(key)) {
			keyMap.put(key, new ArrayList<Controls>());
		}
		keyMap.get(key).add(control);
	}

	public boolean justPressed(Controls action) {
		return statusMap.get(action) == ControlState.JustPressed;
	}

	public boolean active(Controls action) {
		return justPressed(action) || held(action);
	}

	public boolean held(Controls action) {
		return statusMap.get(action) == ControlState.Held;
	}

	public boolean justReleased(Controls action) {
		return statusMap.get(action) == ControlState.JustReleased;
	}

	// Should be called each loop at the *end* of the logic phase
	public void refresh() {
		for (List<Controls> controls : keyMap.values()) {
			for (Controls control : controls) {
				// If we just got a GLFW_RELEASE event, we only want to keep that
				// around for one logic frame.  Afterwards, we reset it to Open.
				if (statusMap.get(control) == ControlState.JustReleased) {
					statusMap.put(control, ControlState.Open);
				}
				// If we just got a GLFW_PRESS event, we only want to keep that
				// around for one logic frame.  Afterwards, we set it to Held.
				// Handling of further GLFW_REPEAT events are technically unnecessary.
				// This will remain in the Held state until we get a GLFW_RELEASE event.
				if (statusMap.get(control) == ControlState.JustPressed) {
					statusMap.put(control, ControlState.Held);
				}
			}
		}
	}

//	public void remap(Controls newControl, Integer key) {
//		keyMap.put(newControl, key);
//	}
//
//	public void consume(Controls action) {
//		KeyboardHelper.consume( keyMap.get(action) );
//	}
}
