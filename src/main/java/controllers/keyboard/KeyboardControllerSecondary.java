package controllers.keyboard;


import controllers.Controls;

import static org.lwjgl.glfw.GLFW.*;

public class KeyboardControllerSecondary extends BaseKeyboardController {

    public KeyboardControllerSecondary(KeyboardControllerManager manager) {
        super(manager);
    }

    @Override
    protected void initKeys() {
        bindKey(GLFW_KEY_LEFT, Controls.LEFT);
        bindKey(GLFW_KEY_RIGHT, Controls.RIGHT);
        bindKey(GLFW_KEY_UP, Controls.UP);
        bindKey(GLFW_KEY_DOWN, Controls.DOWN);

        bindKey(GLFW_KEY_KP_0, Controls.ToggleMovt);
        bindKey(GLFW_KEY_SLASH, Controls.SHRINKSELF);

//		keyMap.put(Controls.JUMP, Keyboard.KEY_J);
//		keyMap.put(Controls.SPRINT, Keyboard.KEY_SEMICOLON);
//		keyMap.put(Controls.CLIMB,  Keyboard.KEY_K);
//		keyMap.put(Controls.USE_ITEM, Keyboard.KEY_I);
//		keyMap.put(Controls.SELECT, Keyboard.KEY_RETURN);
//		keyMap.put(Controls.PAUSE, Keyboard.KEY_ESCAPE);
//		keyMap.put(Controls.DIG, Keyboard.KEY_L);
    }
}
