package controllers;

public enum ControlState {
    JustPressed,
    Held,
    JustReleased,
    Open
};
