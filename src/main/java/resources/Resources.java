package resources;

import java.nio.file.Path;

public class Resources {

    public static String ROOT = "src/main/resources";

    public static String SPRITE_ROOT = Path.of(ROOT, "sprites").toString();
    public static String SHADER_ROOT = Path.of(ROOT, "shaders").toString();
}
