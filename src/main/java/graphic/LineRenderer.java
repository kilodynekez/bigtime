/*
 * The MIT License (MIT)
 *
 * Copyright © 2014-2018, Heiko Brumme
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package graphic;

import geom.Rectangle2D;
import geom.Vector2D;
import math.Matrix4f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import resources.Resources;
import text.Font;

import java.awt.FontFormatException;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;

/**
 * This class is performing the rendering process.
 *
 * @author Heiko Brumme
 */
public class LineRenderer {

    private VertexArrayObject vao;
    private VertexBufferObject vbo;
    private ShaderProgram program;

    private DoubleBuffer vertices;
    private int numVertices;
    private boolean drawing;

    private Font font;
    private Font debugFont;

    int DRAW_TYPE = -1;

    /** Initializes the renderer. */
    public void init() {
        /* Setup shader programs */
        setupShaderProgram();

        /* Enable blending */
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        /* Create fonts */
        try {
            font = new Font(new FileInputStream("resources/Inconsolata.ttf"), 16);
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(LineRenderer.class.getName()).log(Level.CONFIG, null, ex);
            font = new Font();
        }
        debugFont = new Font(12, false);
    }

    /**
     * Clears the drawing area.
     */
    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Begin rendering.
     */
    public void begin() {
        if (drawing) {
            throw new IllegalStateException("Renderer is already drawing!");
        }
        drawing = true;
        numVertices = 0;
        DRAW_TYPE = -1;
    }

    /**
     * End rendering.
     */
    public void end() {
        if (!drawing) {
            throw new IllegalStateException("Renderer isn't drawing!");
        }
        drawing = false;
        flush();
    }

    /**
     * Flushes the data to the GPU to let it get rendered.
     */
    public void flush() {
        if (numVertices > 0) {
            vertices.flip();

            if (vao != null) {
                vao.bind();
            } else {
                vbo.bind(GL_ARRAY_BUFFER);
                specifyVertexAttributes();
            }
            program.use();

            /* Upload the new vertex data */
            vbo.bind(GL_ARRAY_BUFFER);
            vbo.uploadSubData(GL_ARRAY_BUFFER, 0, vertices);



            /* Draw batch */
            glDrawArrays(DRAW_TYPE, 0, numVertices);

            /* Clear vertex data for next batch */
            vertices.clear();
            numVertices = 0;
        }
    }


//    /**
//     * Draws a texture region with the currently bound texture on specified
//     * coordinates.
//     *
//     * @param x1 Bottom left x position
//     * @param y1 Bottom left y position
//     * @param x2 Top right x position
//     * @param y2 Top right y position
//     * @param s1 Bottom left s coordinate
//     * @param t1 Bottom left t coordinate
//     * @param s2 Top right s coordinate
//     * @param t2 Top right t coordinate
//     * @param c  The color to use
//     */
//    public void drawTextureRegion(float x1, float y1, float x2, float y2, float s1, float t1, float s2, float t2, Color c) {
//        if (vertices.remaining() < 8 * 6) {
//            /* We need more space in the buffer, so flush it */
//            flush();
//        }
//
//        float r = c.getRed();
//        float g = c.getGreen();
//        float b = c.getBlue();
//        float a = c.getAlpha();
//
//        vertices.put(x1).put(y1).put(r).put(g).put(b).put(a).put(s1).put(t1);
//        vertices.put(x1).put(y2).put(r).put(g).put(b).put(a).put(s1).put(t2);
//        vertices.put(x2).put(y2).put(r).put(g).put(b).put(a).put(s2).put(t2);
//
//        vertices.put(x1).put(y1).put(r).put(g).put(b).put(a).put(s1).put(t1);
//        vertices.put(x2).put(y2).put(r).put(g).put(b).put(a).put(s2).put(t2);
//        vertices.put(x2).put(y1).put(r).put(g).put(b).put(a).put(s2).put(t1);
//
//        numVertices += 6;
//    }

    public void drawLine(double x1, double y1, double x2, double y2, Color c){
        if (DRAW_TYPE > -1 && DRAW_TYPE != GL_LINES ) {
            throw new RuntimeException("Mismatched DRAW_TYPE");
        }
        DRAW_TYPE = GL_LINES;

        int verticesAdded = 2;
        int vertexDataSize = 6;
        if (vertices.remaining() < verticesAdded * vertexDataSize) {
            /* We need more space in the buffer, so flush it */
            flush();
        }

        double r = c.getRed();
        double g = c.getGreen();
        double b = c.getBlue();
        double a = c.getAlpha();

        vertices.put(x1).put(y1).put(r).put(g).put(b).put(a);
        vertices.put(x2).put(y2).put(r).put(g).put(b).put(a);


        numVertices += verticesAdded;
    }

    public void drawPoly(List<Vector2D> points, Color c) {
        if (DRAW_TYPE > -1 && DRAW_TYPE != GL_LINE_LOOP ) {
            throw new RuntimeException("Mismatched DRAW_TYPE");
        }
        DRAW_TYPE = GL_LINE_LOOP;

        int verticesAdded = points.size();
        int vertexDataSize = 6;
        if (vertices.remaining() < verticesAdded * vertexDataSize) {
            /* We need more space in the buffer, so flush it */
            flush();
        }

        double r = c.getRed();
        double g = c.getGreen();
        double b = c.getBlue();
        double a = c.getAlpha();

        for (Vector2D p : points) {
            vertices.put(p.getX()).put(p.getY()).put(r).put(g).put(b).put(a);
        }

        numVertices += verticesAdded;
    }

    public void drawRect(Rectangle2D rect, Color c) {
        drawPoly(rect.points(), c);
    }

    /**
     * Dispose renderer and clean up its used data.
     */
    public void dispose() {
        MemoryUtil.memFree(vertices);

        if (vao != null) {
            vao.delete();
        }
        vbo.delete();
        program.delete();

        font.dispose();
        debugFont.dispose();
    }

    /** Setups the default shader program. */
    private void setupShaderProgram() {
        /* Generate Vertex Array Object */
        vao = new VertexArrayObject();
        vao.bind();

        /* Generate Vertex Buffer Object */
        vbo = new VertexBufferObject();
        vbo.bind(GL_ARRAY_BUFFER);

        /* Create FloatBuffer */
//        vertices = MemoryUtil.memAllocFloat(4096);
        vertices = MemoryUtil.memAllocDouble(4096);

        /* Upload null data to allocate storage for the VBO */
        long size = vertices.capacity() * Double.BYTES;
        vbo.uploadData(GL_ARRAY_BUFFER, size, GL_DYNAMIC_DRAW);

        /* Initialize variables */
        numVertices = 0;
        drawing = false;

        /* Load shaders */
        Shader vertexShader, fragmentShader;
        vertexShader = Shader.loadShader(GL_VERTEX_SHADER, Resources.SHADER_ROOT + "/line.vert");
        fragmentShader = Shader.loadShader(GL_FRAGMENT_SHADER, Resources.SHADER_ROOT + "/line.frag");

        /* Create shader program */
        program = new ShaderProgram();
        program.attachShader(vertexShader);
        program.attachShader(fragmentShader);
        program.bindFragmentDataLocation(0, "fragColor");
        program.link();
        program.use();

        /* Delete linked shaders */
        vertexShader.delete();
        fragmentShader.delete();

        /* Get width and height of framebuffer */
        long window = GLFW.glfwGetCurrentContext();
        int width, height;
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer widthBuffer = stack.mallocInt(1);
            IntBuffer heightBuffer = stack.mallocInt(1);
            GLFW.glfwGetFramebufferSize(window, widthBuffer, heightBuffer);
            width = widthBuffer.get();
            height = heightBuffer.get();
        }

        /* Specify Vertex Pointers */
        specifyVertexAttributes();

//        /* Set texture uniform */
//        int uniTex = program.getUniformLocation("texImage");
//        program.setUniform(uniTex, 0);   // TODO change 0 to a textureHandle?

//        /* Set model matrix to identity matrix */
//        Matrix4f model = new Matrix4f();
//        int uniModel = program.getUniformLocation("model");
//        program.setUniform(uniModel, model);
//
//        /* Set view matrix to identity matrix */
//        Matrix4f view = new Matrix4f();
//        int uniView = program.getUniformLocation("view");
//        program.setUniform(uniView, view);

        /* Set projection matrix to an orthographic projection */
        Matrix4f projection = Matrix4f.orthographic(0f, width, 0f, height, -1f, 1f);
        int uniProjection = program.getUniformLocation("projection");
        program.setUniform(uniProjection, projection);
    }

    /**
     * Specifies the vertex pointers.
     */
    private void specifyVertexAttributes() {
        /* Specify Vertex Pointer */
        int posAttrib = program.getAttributeLocation("position");
        program.enableVertexAttribute(posAttrib);
        program.pointVertexAttributeDouble(posAttrib, 2, 6 * Double.BYTES, 0);

        /* Specify Color Pointer */
        int colAttrib = program.getAttributeLocation("color");
        program.enableVertexAttribute(colAttrib);
        program.pointVertexAttributeDouble(colAttrib, 4, 6 * Double.BYTES, 2 * Double.BYTES);

//        /* Specify Texture Pointer */
//        int texAttrib = program.getAttributeLocation("texcoord");
//        program.enableVertexAttribute(texAttrib);
//        program.pointVertexAttribute(texAttrib, 2, 8 * Float.BYTES, 6 * Float.BYTES);
    }

}
