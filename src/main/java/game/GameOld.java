package game;//import gui.AGUIDataReceiver;
//import gui.GUIAlign;
//import gui.GUIButton;
//import gui.GUICheckbox;
//import gui.GUIRect;
//import gui.GUISlider;
//import gui.GUIText;
//import gui.GUIWindow;

import controllers.IController;
import controllers.Controls;
import graphic.*;
import math.Matrix4f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.system.MemoryUtil.NULL;

//import menus.AMenu;
//import menus.SelectionMenu;
//import objects.BlockTile;
//import objects.PlatformPlayer;
//import objects.SpaceTile;
//import objects.Tile;
//import objects.TileObjectTorch;
//import objects.TileObjectType;
//import org.newdawn.slick.Color;
//import org.newdawn.slick.TrueTypeFont;
//import org.newdawn.slick.opengl.*;
//import org.newdawn.slick.util.ResourceLoader;
//import drawing.GLHelper;
//import drawing.Sprite;
//
//import ui.UIHelper;
;

public class GameOld {
	private static int scr_x = 640;
	private static int scr_y = 480;

	static String version = "v0.1.0";

	static double timescale = 1;

//	static World world;
//	static Camera camera;


//	static GUIWindow pauseWin;

	static Gamestate gamestate, prevgamestate;

	// The window handle
	private long window;

	static double lastFrame;

	IController controller;

	static VertexArrayObject vao;
	static VertexBufferObject vbo;
	static VertexBufferObject ebo;

	private static Texture texture;
	private Shader vertexShader;
	private Shader fragmentShader;
	private static ShaderProgram program;

	private static int texx, texy;

	static float angle = 0;
	static int uniModelTriangle;

//	static ArrayList<GUIWindow> mainWindows, pauseWindows;


//	static AMenu optionsMenu;

//	TrueTypeFont font;

	public void start() {
		initOpenGL(scr_x, scr_y);
		init();


		getDelta(); // call once before loop to initialise lastFrame

		loop();

		exit();

		// Cleanup after main loop is terminated
		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();

	}

	private void loop() {


		while (!glfwWindowShouldClose(window)) {
			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();

			// convert milliseconds to seconds
			// double delta measured in seconds
			double delta = getDelta();

//			logic(world, delta);
			logic(delta);

			//controller.refresh();

			glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			render();
			glfwSwapBuffers(window);

//			Display.update();
//			Display.sync(60);

		}
	}



	public void initTextures() {
		/* Get width and height of framebuffer */
		int width, height;
		try (MemoryStack stack = MemoryStack.stackPush()) {
			long window = GLFW.glfwGetCurrentContext();
			IntBuffer widthBuffer = stack.mallocInt(1);
			IntBuffer heightBuffer = stack.mallocInt(1);
			GLFW.glfwGetFramebufferSize(window, widthBuffer, heightBuffer);
			width = widthBuffer.get();
			height = heightBuffer.get();
		}

		/* Create texture */
		texture = Texture.loadTexture("src/main/resources/asdfas-46px.png");
//		texture.bind();

		/* Generate Vertex Array Object */
		vao = new VertexArrayObject();
		vao.bind();

//		/* Get coordinates for centering the texture on screen */
//		float x1 = (width - texture.getWidth()) / 2f;
//		float y1 = (height - texture.getHeight()) / 2f;
//		float x2 = x1 + texture.getWidth();
//		float y2 = y1 + texture.getHeight();

		/* Get coordinates for centering the texture on screen */
		float x1 = (0 - texture.getWidth()) / 2f;
		float y1 = (0 - texture.getHeight()) / 2f;
		float x2 = x1 + texture.getWidth();
		float y2 = y1 + texture.getHeight();

		try (MemoryStack stack = MemoryStack.stackPush()) {
			/* Vertex data */
			FloatBuffer vertices = stack.mallocFloat(4 * 7);
			// X Y    R G B    S T
			vertices.put(x1).put(y1).put(1f).put(1f).put(1f).put(0f).put(0f);
			vertices.put(x2).put(y1).put(1f).put(1f).put(1f).put(1f).put(0f);
			vertices.put(x2).put(y2).put(1f).put(1f).put(1f).put(1f).put(1f);
			vertices.put(x1).put(y2).put(1f).put(1f).put(1f).put(0f).put(1f);
			vertices.flip();

			/* Generate Vertex Buffer Object */
			vbo = new VertexBufferObject();
			vbo.bind(GL_ARRAY_BUFFER);
			vbo.uploadData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

			/* Element data */
			IntBuffer elements = stack.mallocInt(2 * 3);
			elements.put(0).put(1).put(2);
			elements.put(2).put(3).put(0);
			elements.flip();

			/* Generate Element Buffer Object */
			ebo = new VertexBufferObject();
			ebo.bind(GL_ELEMENT_ARRAY_BUFFER);
			ebo.uploadData(GL_ELEMENT_ARRAY_BUFFER, elements, GL_STATIC_DRAW);
		}

		/* Load shaders */
//		vertexShader = Shader.loadShader(GL_VERTEX_SHADER, "resources/default.vert");
//		fragmentShader = Shader.loadShader(GL_FRAGMENT_SHADER, "resources/default.frag");
		vertexShader = Shader.loadShader(GL_VERTEX_SHADER,     "src/main/resources/shaders/vertexShader.gsls");
		fragmentShader = Shader.loadShader(GL_FRAGMENT_SHADER, "src/main/resources/shaders/fragmentShader.gsls");

		/* Create shader program */
		program = new ShaderProgram();
		program.attachShader(vertexShader);
		program.attachShader(fragmentShader);
		program.bindFragmentDataLocation(0, "fragColor");
		program.link();
		program.use();

		specifyVertexAttributes();

		/* Set texture uniform */
		int uniTex = program.getUniformLocation("texImage");
		program.setUniform(uniTex, 0);

		/* Set model matrix to identity matrix */
		// The model matrix will calculate the local object coordinates to the world coordinates.
		// In OpenGL you use it for example for scaling, translating or rotating the object.
		Matrix4f model = new Matrix4f();
		int uniModel = program.getUniformLocation("model");
		program.setUniform(uniModel, model);

		/* Set view matrix to identity matrix */
		// From the world coordinates you use the view matrix to calculate the eye coordinates, this is used for the camera position.
		// But you won't move the camera, instead you move the world, so to get the correct view matrix you have to transform the world
		// with the inverse of the camera transformation.
		Matrix4f view = new Matrix4f();
		int uniView = program.getUniformLocation("view");
		program.setUniform(uniView, view);

		/* Set projection matrix to an orthographic projection */
		// Finally the projection will take the eye coordinates and calculates the clip coordinates, you use that matrix to apply an
		// orthographic or a perspective matrix.
		Matrix4f projection = Matrix4f.orthographic(0f, width, 0f, height, -1f, 1f);
		int uniProjection = program.getUniformLocation("projection");
		program.setUniform(uniProjection, projection);

		// important note about calculation the MVP matrix is that you have to multiply those three matrices in reversed order.
		// This is because how the vector and matrix calculation works: newVector = projection * view * model * vector,
		// so actually it gets calculated like this: newVector = projection * (view * (model * vector)).
	}

	/**
	 * Specifies the vertex attributes.
	 */
	private void specifyVertexAttributes() {
		/* Specify Vertex Pointer */
		int posAttrib = program.getAttributeLocation("position");
		program.enableVertexAttribute(posAttrib);
		program.pointVertexAttributeFloat(posAttrib, 2, 7 * Float.BYTES, 0);

		/* Specify Color Pointer */
		int colAttrib = program.getAttributeLocation("color");
		program.enableVertexAttribute(colAttrib);
		program.pointVertexAttributeFloat(colAttrib, 3, 7 * Float.BYTES, 2 * Float.BYTES);

		/* Specify Texture Pointer */
		int texAttrib = program.getAttributeLocation("texcoord");
		program.enableVertexAttribute(texAttrib);
		program.pointVertexAttributeFloat(texAttrib, 2, 7 * Float.BYTES, 5 * Float.BYTES);
	}

	public void initOpenGL(int width, int height) {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the window will not be resizable

//		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

		// Create the window
		window = glfwCreateWindow(width, height, "BigTime " + version, NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		// Center the window
		glfwSetWindowPos(
				window,
				(vidmode.width() - width) / 2,
				(vidmode.height() - height) / 2
		);


		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();

		System.out.format("Getting Version and Vendor info\n");
		System.out.format("GL Ver: %s\n",glGetString(GL_VERSION));
		System.out.format("GL Vendor: %s\n" , glGetString(GL_VENDOR));


		System.out.format("Initializing texture setup\n");
		initTextures();


//		// TEXTURES
//		int texture = glGenTextures();
//		glBindTexture(GL_TEXTURE_2D, texture);
//
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//
//		try(MemoryStack stack = stackPush()) {
//			IntBuffer w = stack.mallocInt(1);
//			IntBuffer h = stack.mallocInt(1);
//			IntBuffer comp = stack.mallocInt(1);
//			//2919 x 3972
//			ByteBuffer image = stbi_load("src/main/resources/asdfas.png", w, h, comp, 4);
//
//			if (image == null) {
//				throw new RuntimeException("Failed to load a texture file!"
//						+ System.lineSeparator() + stbi_failure_reason());
//			}
//
//			int imgwidth = w.get();
//			int imgheight = h.get();
//
//			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, imgwidth, imgheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
//
//			int texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
//			glEnableVertexAttribArray(texAttrib);
//			glVertexAttribPointer(texAttrib, 2, GL_FLOAT, false, 7*floatSize, 6 * floatSize);
//
//			// From Tutorial https://github.com/SilverTiger/lwjgl3-tutorial/wiki/Textures
//			// And of course we need to set the new uniform variable, but with one texture this is completely optional,
//			// because the texture gets bound to value 0 by default. This is more important if you want to have multiple
//			// textures in your shader.
//			int uniTex = glGetUniformLocation(shaderProgram, "texImage");
//			glUniform1f(uniTex, 0);
//
//			int ebo = glGenBuffers();
//			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
//
//			IntBuffer elements = stack.mallocInt(2 * 3);
//			elements.put(0).put(1).put(2);
//			elements.put(2).put(3).put(0);
//			elements.flip();
//
//			glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements, GL_STATIC_DRAW);
//
////			long window = GLFW.glfwGetCurrentContext();
//			IntBuffer widthBuffer = stack.mallocInt(1);
//			IntBuffer heightBuffer = stack.mallocInt(1);
//			GLFW.glfwGetFramebufferSize(window, widthBuffer, heightBuffer);
//			int winwidth = widthBuffer.get();
//			int winheight = heightBuffer.get();
//
//			float x1 = (winwidth - imgwidth) / 2f;
//			float y1 = (winheight - imgheight) / 2f;
//			float x2 = x1 + imgwidth;
//			float y2 = y1 + imgheight;
//
//			FloatBuffer vertices = stack.mallocFloat(4 * 7);
//			vertices.put(x1).put(y1).put(1f).put(1f).put(1f).put(0f).put(0f);
//			vertices.put(x2).put(y1).put(1f).put(1f).put(1f).put(1f).put(0f);
//			vertices.put(x2).put(y2).put(1f).put(1f).put(1f).put(1f).put(1f);
//			vertices.put(x1).put(y2).put(1f).put(1f).put(1f).put(0f).put(1f);
//			vertices.flip();
//
//			int vbo = glGenBuffers();
//			glBindBuffer(GL_ARRAY_BUFFER, vbo);
//			glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
//
//
//			/// TODO Continue from here...
//
//		}

//		GL11.glEnable(GL11.GL_TEXTURE_2D);
//		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
//
//        	// enable alpha blending
//        	GL11.glEnable(GL11.GL_BLEND);
//        	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//
//        	GL11.glViewport(0,0,width,height);
//		GL11.glMatrixMode(GL11.GL_MODELVIEW);
//
//		GL11.glMatrixMode(GL11.GL_PROJECTION);
//		GL11.glLoadIdentity();
//		GL11.glOrtho(0, width, height, 0, 1, -1);
//		GL11.glMatrixMode(GL11.GL_MODELVIEW);


	}

	/*
	 * Initialize resources
	 */
	public void init() {
		gamestate = Gamestate.TITLE;

		//Init controller
//		controller = new BaseKeyboardController(window);

		/*

		//Setup Gamepad helper
//		GamepadHelper.init();


		world = new World();
		world.loadMap("res/Map2.mp1");

		Texture texture = null;
		Texture blockT = null;
		Texture blinky = null;
		try {
			// load texture from PNG file
			blockT = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/image.png"));
			blinky = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/AniStone.png"));
			tileBorder = new Sprite(TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/TileBorder.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//create Sprite from texture
		world.player = new PlatformPlayer(controller, world.getMap().playerLoc, 6, 10 );
		world.player.loadResources();

		Sprite blockS = new Sprite(blockT);
		//world.blocks = new ArrayList<WorldBlock>();
		//world.blocks.add( new WorldBlock( new Vector2D(300, 300), BlockType.STONE, blockS));

//		 Init UIHelper
		UIHelper.init();

//		 Setup Camera
		camera = new Camera(0, 0, getScr_x(), scr_y, font);

//		Setup Options menu
//		optionsMenu = new SelectionMenu("Options", "Dummy 1", "Dummy 2", "Exit");

//		 Setup windows active during the main gamestate
		mainWindows = new ArrayList<GUIWindow>();

//		 Setup windows active during the pause gamestate
		pauseWindows = new ArrayList<GUIWindow>();

		pauseWin = new GUIWindow(new Vector2D(100,100), 200, 300);

		GUIRect pauseBG = new GUIRect(pauseWin.getSize().getXi(), pauseWin.getSize().getYi(), new Color(0, 0, 0, 230));
		pauseWin.addComponent(pauseBG, new Vector2D(0, 0));

		GUIText pauseText = new GUIText("PAUSE");
		pauseText.setAlignment(GUIAlign.CENTER);
		pauseWin.addComponent(pauseText, new Vector2D(pauseWin.getSize().getXi()/2, 0));

		AGUIDataReceiver<String> torchlightRcvr = new AGUIDataReceiver<String>() {

			@Override
			public void update(String id, Object arg) {
				Color col = TileObjectTorch.lightColor;

				if (id.equals("Draw"))
					Camera.setLighting((boolean) arg);
				else if (id.equals("SetColorR"))
					TileObjectTorch.lightColor = new Color((int)arg / 255f, col.g, col.b, col.a);
				else if (id.equals("SetColorG"))
					TileObjectTorch.lightColor = new Color(col.r, (int)arg / 255f, col.b, col.a);
				else if (id.equals("SetColorB"))
					TileObjectTorch.lightColor = new Color(col.r, col.g, (int)arg / 255f, col.a);
				else if (id.equals("SetColorA"))
					TileObjectTorch.lightColor = new Color(col.r, col.g, col.b, (int)arg / 255f);
				else
					System.err.format("torchlightRcvr received unrecognized update ID: \"%s\"\n", id);
			}

		};

		GUICheckbox pauseCB = new GUICheckbox("Draw lighting", false);
		torchlightRcvr.registerDataInput("Draw", pauseCB);
		pauseWin.addComponent(pauseCB, new Vector2D(10, 50));

		GUISlider pauseSliderR = new GUISlider(0, 255, 0);
		pauseSliderR.setFillColor(Color.red);
		torchlightRcvr.registerDataInput("SetColorR", pauseSliderR);
		pauseWin.addComponent(pauseSliderR, new Vector2D(10, 100));

		GUISlider pauseSliderG = new GUISlider(0, 255, 0);
		pauseSliderG.setFillColor(Color.green);
		torchlightRcvr.registerDataInput("SetColorG", pauseSliderG);
		pauseWin.addComponent(pauseSliderG, new Vector2D(10, 120));

		GUISlider pauseSliderB = new GUISlider(0, 255, 0);
		pauseSliderB.setFillColor(Color.blue);
		torchlightRcvr.registerDataInput("SetColorB", pauseSliderB);
		pauseWin.addComponent(pauseSliderB, new Vector2D(10, 140));



		GUIButton pauseButton = new GUIButton("button");
		pauseWin.addComponent(pauseButton, new Vector2D(30, 200));

		pauseWin.setActive(true);
		pauseWindows.add(pauseWin);
		 */
	}

	/**
	 * Calculate how many milliseconds have passed
	 * since last frame.
	 *
	 * @return milliseconds passed since last frame
	 */
	public static double getDelta() {
	    double time = glfwGetTime();
	    double delta = (time - lastFrame);
	    lastFrame = time;

	    return delta;
	}

	//game.Game Logic
//	public void logic(World world, double delta) {
	public void logic(double delta) {
		angle += delta * 50f;

		if (controller.held(Controls.LEFT)) {

		}

//		System.out.format("Running logic: %f\n", angle);
//		KeyboardHelper.refresh();
//		MouseHelper.refresh();


//		if (gamestate == game.Gamestate.PAUSED) {
//
//			for (GUIWindow win : pauseWindows) {
//				if (win.isActive())
//					win.update();
//			}
//
//			if (controller.pressed(Controls.PAUSE)) {
//				changeGamestate(prevgamestate);
//				controller.consume(Controls.PAUSE);
//			}
//
//		}
//
//		if (gamestate == game.Gamestate.TITLE) {
//			if(KeyboardHelper.pressed(Keyboard.KEY_RETURN))
//				changeGamestate(game.Gamestate.MAIN);
//
//			/*-- Options menu --*/
//			if(KeyboardHelper.pressed(Keyboard.KEY_SPACE)) {
//				String result = executeMenu(new SelectionMenu("Options", "Change Controls", "Exit"));
//				System.out.format("Chose %s\n", result);
//
//
//			}
//		}
//
//		if (gamestate == game.Gamestate.MAIN || gamestate == game.Gamestate.DEAD) {
//
//
//			if (controller.pressed(Controls.PAUSE)) {
//				changeGamestate(game.Gamestate.PAUSED);
//			}
//
//
//			int mx = Mouse.getX() - camera.offset().getXi();
//			int my = scr_y - Mouse.getY() - camera.offset().getYi();
//			int mw = Mouse.getDWheel() / 120;
//
//			if (mx > 0 && mx < world.getMap().width() * world.getMap().scale() &&
//					my > 0 && mx < world.getMap().height() * world.getMap().scale() ) {
//
//				selectedTile = world.getMap().tileAt(mx, my);
//
//			}
//
//			if (selectedTile != null) {
//
//				if (MouseHelper.click(0)) {
//					if( selectedTile instanceof SpaceTile ) {
//						SpaceTile selected = (SpaceTile)selectedTile;
//						if (selected.getObject() == null) {
//							new TileObjectTorch(selected);
//						}
//					}
//					if( selectedTile instanceof BlockTile ) {
//						BlockTile selected = (BlockTile)selectedTile;
//						selected.destroy();
//					}
//				}
//
//				if (MouseHelper.click(1)) {
//					if( selectedTile instanceof SpaceTile ) {
//						SpaceTile selected = (SpaceTile)selectedTile;
//						if (selected.getObject() != null && selected.getObject().getType() == TileObjectType.TORCH) {
//							TileObjectTorch torch = (TileObjectTorch) selected.getObject();
//
//							torch.destroy();
//						}
//
//					}
//				}
//
//			}
//
//			world.update(delta);
//			camera.update(delta, world);
//		}

//		if (gamestate == game.Gamestate.DEAD) {
//			if(KeyboardHelper.pressed(Keyboard.KEY_SPACE)) {
//				changeGamestate(game.Gamestate.MAIN);
//				world.player.resurrect();
//			}
//		}

	}

	public static void render() {
		// rotate the model

		Matrix4f textureModelMatrix = new Matrix4f();

		// T * R * S
		textureModelMatrix =
			Matrix4f.translate(200f, 200f,0)
			.multiply(
			Matrix4f.rotate(angle, 0f,0f,1)
			)
			.multiply(
			Matrix4f.scale(2, 2, 1)
			)
			.multiply(textureModelMatrix)
		;

		int uniModel = program.getUniformLocation("model");
		program.setUniform(uniModel, textureModelMatrix);

		vao.bind();
		texture.bind();
		program.use();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

//		Color txtCol = new Color(130,0,130);
//		GL11.glColor4f( 1f, 1f, 1f, 1f);

//		if (gamestate == game.Gamestate.TITLE) {
//			UIHelper.drawStringCentered(scr_x/2, 200, "Press [Enter] to begin", Color.red);
//			UIHelper.drawStringCentered(scr_x/2, 220, "Press [Space] for Options", Color.red);
//			UIHelper.drawStringCentered(scr_x/2, 250, "v0.01", Color.red);
//		}

//		if (gamestate == game.Gamestate.MAIN || gamestate == game.Gamestate.PAUSED) {
//			/* Draw the world */
//			camera.draw(world);
//
//			/* Draw debug info */
//			Color.white.bind();
//			UIHelper.drawString(400, 10, "Map: " + world.getMap().name(), txtCol);
//			UIHelper.drawString(400, 25, world.getMap().width() + "x" + world.getMap().height() + "  scale: " + world.getMap().scale(),
//					                 txtCol);
//
//			UIHelper.drawString(400, 55, "Player Health: " + world.player.getHp(), txtCol);
//			UIHelper.drawString(400, 70, "Player State: " + world.player.getState().toString(), txtCol);
//			//UIHelper.drawString(400, 70, "Player speedFactor: " + world.player.speedFactor, txtCol);
//			UIHelper.drawString(400, 85, "Player velY: " + (world.player.getVel().getY() < 500 ? world.player.getVel().getY() : "MAX!"), txtCol);
//
//			UIHelper.drawString(400, 115, "Screen X: " + camera.bounds.getPos().getX() + ", Y: " + camera.bounds.getPos().getY(), txtCol);
//
//			UIHelper.drawString(400, 145, "timescale: " + timescale, txtCol);
//			//------
//
//			//Draw Tile Border
//			if( selectedTile != null) {
//				tileBorder.draw(new Vector2D(selectedTile.getLocx() * Tile.getScale(), selectedTile.getLocy() * Tile.getScale()).add(camera.offset()), new Color(0,255,0) );
//
//				UIHelper.drawString(400, 170, "Selected Tile Information: ", txtCol);
//				UIHelper.drawString(420, 185, "Location: " + selectedTile.getLocx() + ", " + selectedTile.getLocy(), txtCol);
//				UIHelper.drawString(420, 200, "Class: " + selectedTile.getClass().getName(), txtCol);
//
//				if( selectedTile instanceof SpaceTile ) {
//					SpaceTile tile = (SpaceTile)selectedTile;
//					//if( tile.object != null )
//						UIHelper.drawString(420, 215, "Contains: " + (tile.getObject() == null ? "Null" : tile.getObject().getClass().getName()), txtCol);
//				}
//
//
//				UIHelper.drawString(420, 230, "lightLevel: " + selectedTile.getLightLevel(), txtCol);
//			}
//		}
//
//		if (gamestate == game.Gamestate.DEAD) {
//			/* Draw the world */
//			camera.draw(world);
//
//			UIHelper.drawStringCentered(scr_x/2, 200, "You have died. Press [Space] to continue", Color.red);
//		}
//
//		if (gamestate == game.Gamestate.PAUSED) {
//			//GLHelper.rectfill(0, 0, scr_x, scr_y, new Color(0,0,0,200));
//
//			for (GUIWindow win : pauseWindows) {
//				if (win.isActive())
//					win.draw();
//			}
//		}

	}

	public static void main(String[] argv){
		GameOld game = new GameOld();
		game.start();

		return;
	}

	public static void playerDead() {
		changeGamestate(Gamestate.DEAD);
	}

//	public static String executeMenu(AMenu menu) {
//		String response = null;
//
//		while (response == null) {
//			KeyboardHelper.refresh();
//			response = menu.update();
//
//			double delta = getDelta();
//
//			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
//			render();
//			menu.render();
//
//			Display.update();
//			Display.sync(60);
//
//			if (Display.isCloseRequested()) {
//				Display.destroy();
//				System.exit(0);
//			}
//		}
//
//		return response;
//	}


	public static void changeGamestate(Gamestate newstate) {
		prevgamestate = gamestate;
		gamestate = newstate;
	}


	public void exit() {
		vao.delete();
		vbo.delete();
		ebo.delete();
		texture.delete();
		vertexShader.delete();
		fragmentShader.delete();
		program.delete();
	}
}
