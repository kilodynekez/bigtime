package game;

//import gui.AGUIDataReceiver;
//import gui.GUIAlign;
//import gui.GUIButton;
//import gui.GUICheckbox;
//import gui.GUIRect;
//import gui.GUISlider;
//import gui.GUIText;
//import gui.GUIWindow;
import animation.Animation;
import controllers.*;
import controllers.keyboard.KeyboardControllerManager;
import controllers.keyboard.KeyboardControllerPrimary;
import controllers.keyboard.KeyboardControllerSecondary;
import entities.BoundingBox;
import entities.Entity;
import entities.FloorTileEntity;
import entities.physics.MicroEntity;
import entities.physics.PhysicsEntity;
import entities.physics.PlayerEntity;
import entities.characters.Lisa;
import entities.characters.LisaGhost;
import entities.states.PostureState;
import events.AttackEvent;
import geom.Vector2D;
import graphic.*;
//import menus.AMenu;
//import menus.SelectionMenu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import objects.BlockTile;
//import objects.PlatformPlayer;
//import objects.SpaceTile;
//import objects.Tile;
//import objects.TileObjectTorch;
//import objects.TileObjectType;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

		import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
		import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.stb.STBImage.stbi_load;
		import static org.lwjgl.system.MemoryUtil.NULL;


//import org.newdawn.slick.Color;
//import org.newdawn.slick.TrueTypeFont;
//import org.newdawn.slick.opengl.*;
//import org.newdawn.slick.util.ResourceLoader;


//import drawing.GLHelper;
//import drawing.Sprite;
//
//import ui.UIHelper;


enum Gamestate {TITLE, MAIN, MENU, DEAD, PAUSED};

public class Game {
	private static int scr_x = 640;
	private static int scr_y = 480;

	static String version = "v0.1.0";

	static double timescale = 1;

//	static World world;
//	static Camera camera;


//	static GUIWindow pauseWin;

	static Gamestate gamestate, prevgamestate;

	// The window handle
	private long window;

	static double lastFrame;

	IController controller;

	static VertexArrayObject vao;
	static VertexBufferObject vbo;
	static VertexBufferObject ebo;

	private static Texture texture;
	private Shader vertexShader;
	private Shader fragmentShader;
	private static ShaderProgram program;

	private static int texx, texy;

	static int uniModelTriangle;

//	static Renderer renderer;
//	static LineRenderer lineRenderer;
	static RenderWrapper renderer;

	static PlayerEntity player;
	private static PlayerEntity player2;
	private static Map<String, Animation> animations;
	private IController controller2;

	// TODO Generalize to TileEntites
	private static List<FloorTileEntity> tileEntities;

	List<PlayerEntity> activePlayers;
	private static ArrayList<MicroEntity> micros;

//	static ArrayList<GUIWindow> mainWindows, pauseWindows;


//	static AMenu optionsMenu;

//	TrueTypeFont font;

	public void start() {
		initOpenGL(scr_x, scr_y);

		renderer = new RenderWrapper();
		renderer.init();

//		lineRenderer = new LineRenderer();
//		lineRenderer.init();

		texture = Texture.loadTexture("src/main/resources/asdfas-46px.png");

		try {
			init();
			getDelta(); // call once before loop to initialise lastFrame

			loop();

			exit();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {

			// Cleanup after main loop is terminated
			// Free the window callbacks and destroy the window
			glfwFreeCallbacks(window);
			glfwDestroyWindow(window);

			// Terminate GLFW and free the error callback
			glfwTerminate();
			glfwSetErrorCallback(null).free();
		}




	}

	private void loop() {


		while (!glfwWindowShouldClose(window)) {
			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();

			// convert milliseconds to seconds
			// double delta measured in seconds
			double delta = getDelta();

//			logic(world, delta);
			logic(delta);

			//controller.refresh();

//			glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			render(delta);
			glfwSwapBuffers(window);

//			Display.update();
//			Display.sync(60);

		}
	}


	public void initOpenGL(int width, int height) {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the window will not be resizable

//		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

		// Create the window
		window = glfwCreateWindow(width, height, "BigTime " + version, NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		// Center the window
		glfwSetWindowPos(
				window,
				(vidmode.width() - width) / 2,
				(vidmode.height() - height) / 2
		);


		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();

		System.out.format("Getting Version and Vendor info\n");
		System.out.format("GL Ver: %s\n",glGetString(GL_VERSION));
		System.out.format("GL Vendor: %s\n" , glGetString(GL_VENDOR));

	}

	/*
	 * Initialize resources
	 */
	public void init() throws IOException {
		gamestate = Gamestate.TITLE;

		// Init keyboard controller manager
		KeyboardControllerManager keyboardManager = new KeyboardControllerManager(window);

		activePlayers = new ArrayList<>();

		//Init default keybord controller
		controller = new KeyboardControllerPrimary(keyboardManager);
		player = new Lisa(
				controller,
				new Vector2D(16*4,400)
		);
		activePlayers.add(player);

		controller2 = new KeyboardControllerSecondary(keyboardManager);
		player2 = new LisaGhost(
				controller2,
				new Vector2D(16*6,400)
		);
		activePlayers.add(player2);

		tileEntities = new ArrayList<>();
		int tilex;
		for (tilex = 16*3; tilex < 16*(3+5); tilex += 16) {
			tileEntities.add(new FloorTileEntity(new Vector2D(tilex,48+256), FloorTileEntity.TileEntityType.OUTER_TOP));
		}
		for (tilex = 16*3; tilex < 16*(3+10); tilex += 16) {
			tileEntities.add(new FloorTileEntity(new Vector2D(tilex,48+128), FloorTileEntity.TileEntityType.OUTER_TOP));
		}
		for (tilex = 16*3; tilex < scr_x - 16*3; tilex += 16) {
			tileEntities.add(new FloorTileEntity(new Vector2D(tilex,48), FloorTileEntity.TileEntityType.OUTER_TOP));
		}
		for (tilex = 16*0; tilex < scr_x - 16; tilex += 16) {
			tileEntities.add(new FloorTileEntity(new Vector2D(tilex,16), FloorTileEntity.TileEntityType.OUTER_TOP));
		}
		tileEntities.add(new FloorTileEntity(new Vector2D(0,32), FloorTileEntity.TileEntityType.OUTER_TOP));
		tileEntities.add(new FloorTileEntity(new Vector2D(scr_x-16,32), FloorTileEntity.TileEntityType.OUTER_TOP));

		micros = new ArrayList<>();
		micros.add(new MicroEntity(new Vector2D(300, scr_y-50)));
		micros.add(new MicroEntity(new Vector2D(350, scr_y-50)));
		micros.add(new MicroEntity(new Vector2D(400, scr_y-50)));
		micros.add(new MicroEntity(new Vector2D(450, scr_y-50)));
		micros.add(new MicroEntity(new Vector2D(500, scr_y-50)));
		micros.add(new MicroEntity(new Vector2D(550, scr_y-50)));

		/*

		//Setup Gamepad helper
//		GamepadHelper.init();


		world = new World();
		world.loadMap("res/Map2.mp1");

		Texture texture = null;
		Texture blockT = null;
		Texture blinky = null;
		try {
			// load texture from PNG file
			blockT = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/image.png"));
			blinky = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/AniStone.png"));
			tileBorder = new Sprite(TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/TileBorder.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//create Sprite from texture
		world.player = new PlatformPlayer(controller, world.getMap().playerLoc, 6, 10 );
		world.player.loadResources();

		Sprite blockS = new Sprite(blockT);
		//world.blocks = new ArrayList<WorldBlock>();
		//world.blocks.add( new WorldBlock( new Vector2D(300, 300), BlockType.STONE, blockS));

//		 Init UIHelper
		UIHelper.init();

//		 Setup Camera
		camera = new Camera(0, 0, getScr_x(), scr_y, font);

//		Setup Options menu
//		optionsMenu = new SelectionMenu("Options", "Dummy 1", "Dummy 2", "Exit");

//		 Setup windows active during the main gamestate
		mainWindows = new ArrayList<GUIWindow>();

//		 Setup windows active during the pause gamestate
		pauseWindows = new ArrayList<GUIWindow>();

		pauseWin = new GUIWindow(new Vector2D(100,100), 200, 300);

		GUIRect pauseBG = new GUIRect(pauseWin.getSize().getXi(), pauseWin.getSize().getYi(), new Color(0, 0, 0, 230));
		pauseWin.addComponent(pauseBG, new Vector2D(0, 0));

		GUIText pauseText = new GUIText("PAUSE");
		pauseText.setAlignment(GUIAlign.CENTER);
		pauseWin.addComponent(pauseText, new Vector2D(pauseWin.getSize().getXi()/2, 0));

		AGUIDataReceiver<String> torchlightRcvr = new AGUIDataReceiver<String>() {

			@Override
			public void update(String id, Object arg) {
				Color col = TileObjectTorch.lightColor;

				if (id.equals("Draw"))
					Camera.setLighting((boolean) arg);
				else if (id.equals("SetColorR"))
					TileObjectTorch.lightColor = new Color((int)arg / 255f, col.g, col.b, col.a);
				else if (id.equals("SetColorG"))
					TileObjectTorch.lightColor = new Color(col.r, (int)arg / 255f, col.b, col.a);
				else if (id.equals("SetColorB"))
					TileObjectTorch.lightColor = new Color(col.r, col.g, (int)arg / 255f, col.a);
				else if (id.equals("SetColorA"))
					TileObjectTorch.lightColor = new Color(col.r, col.g, col.b, (int)arg / 255f);
				else
					System.err.format("torchlightRcvr received unrecognized update ID: \"%s\"\n", id);
			}

		};

		GUICheckbox pauseCB = new GUICheckbox("Draw lighting", false);
		torchlightRcvr.registerDataInput("Draw", pauseCB);
		pauseWin.addComponent(pauseCB, new Vector2D(10, 50));

		GUISlider pauseSliderR = new GUISlider(0, 255, 0);
		pauseSliderR.setFillColor(Color.red);
		torchlightRcvr.registerDataInput("SetColorR", pauseSliderR);
		pauseWin.addComponent(pauseSliderR, new Vector2D(10, 100));

		GUISlider pauseSliderG = new GUISlider(0, 255, 0);
		pauseSliderG.setFillColor(Color.green);
		torchlightRcvr.registerDataInput("SetColorG", pauseSliderG);
		pauseWin.addComponent(pauseSliderG, new Vector2D(10, 120));

		GUISlider pauseSliderB = new GUISlider(0, 255, 0);
		pauseSliderB.setFillColor(Color.blue);
		torchlightRcvr.registerDataInput("SetColorB", pauseSliderB);
		pauseWin.addComponent(pauseSliderB, new Vector2D(10, 140));



		GUIButton pauseButton = new GUIButton("button");
		pauseWin.addComponent(pauseButton, new Vector2D(30, 200));

		pauseWin.setActive(true);
		pauseWindows.add(pauseWin);
		 */
	}

	/**
	 * Calculate how many milliseconds have passed
	 * since last frame.
	 *
	 * @return milliseconds passed since last frame
	 */
	public static double getDelta() {
	    double time = glfwGetTime();
	    double delta = (time - lastFrame);
	    lastFrame = time;

	    return delta;
	}

	//game.Game Logic
//	public void logic(World world, double delta) {
	public void logic(double delta) {
		//angle += delta * 50f;

		if (controller.justPressed(Controls.ToggleDebugView)) {
			renderer.toggleDebug();
		}

		// get player input and intial state changes
		// (intended actions)
		for (PlayerEntity player : activePlayers) {
			player.update(delta);
		}

		// Update general entites too
		for (MicroEntity micro : micros) {
			micro.update(delta);
		}
		for (FloorTileEntity tile : tileEntities) {
			tile.update(delta);
		}

		// Check collisions between other world entites
		// TODO Generalize
		List<PhysicsEntity> collidableEntites = new ArrayList<PhysicsEntity>();
		collidableEntites.addAll(micros);
		collidableEntites.addAll(Arrays.asList(player, player2));
		for (PhysicsEntity entity : collidableEntites) {
			boolean collisionWithFloor = false;
//			boolean noFloorUnderneath = true;

			BoundingBox offsetBelow = entity.getHitbox().copy();
			offsetBelow.updatePosition(entity.getPos().add(0, -1));

			// check collissions with scenery
			for (FloorTileEntity tile : tileEntities) {
				// check if we are colliding, if so, move us out and set us as standing
				if (tile.getHitbox().overlaps(entity.getHitbox())) {
					collisionWithFloor = true;
//					noFloorUnderneath = false;
					// Determine penetration
					double penetration = tile.getPos().getY() + tile.getHitbox().getBounds().getHeight() - entity.getPos().getY();
					if (penetration > 0){
						entity.setPos(new Vector2D(entity.getPos().getX(), entity.getPos().getY() + penetration));
					}
					entity.collideWithFloor();
				}

			}

			// If we never collided with a floor, then we must be in the air -- set our state as such
			if (!collisionWithFloor) {
				entity.enterFreefall();
			}
		}

		// After players have updated their inputs and states,
		// see if they interact with each other or the world



		// See if there are active attacks, and if they affect other entities/players
		for (PlayerEntity player : activePlayers) {
			for (AttackEvent event : player.initiatedAttackEvents()) {
				// Now get all entities that could be hurt by this attack
				List<Entity> targets = new ArrayList<>();
				targets.addAll(activePlayers.stream().filter(t -> t != player).toList());
				targets.addAll(micros);

				// Apply the event to all potential targets
				event.apply(targets);
			}
		}

		// Refresh controller inputs after each logic cycle
		controller.refresh();
		controller2.refresh();

//		System.out.format("Running logic: %f\n", angle);
//		KeyboardHelper.refresh();
//		MouseHelper.refresh();


//		if (gamestate == game.Gamestate.PAUSED) {
//
//			for (GUIWindow win : pauseWindows) {
//				if (win.isActive())
//					win.update();
//			}
//
//			if (controller.pressed(Controls.PAUSE)) {
//				changeGamestate(prevgamestate);
//				controller.consume(Controls.PAUSE);
//			}
//
//		}
//
//		if (gamestate == game.Gamestate.TITLE) {
//			if(KeyboardHelper.pressed(Keyboard.KEY_RETURN))
//				changeGamestate(game.Gamestate.MAIN);
//
//			/*-- Options menu --*/
//			if(KeyboardHelper.pressed(Keyboard.KEY_SPACE)) {
//				String result = executeMenu(new SelectionMenu("Options", "Change Controls", "Exit"));
//				System.out.format("Chose %s\n", result);
//
//
//			}
//		}
//
//		if (gamestate == game.Gamestate.MAIN || gamestate == game.Gamestate.DEAD) {
//
//
//			if (controller.pressed(Controls.PAUSE)) {
//				changeGamestate(game.Gamestate.PAUSED);
//			}
//
//
//			int mx = Mouse.getX() - camera.offset().getXi();
//			int my = scr_y - Mouse.getY() - camera.offset().getYi();
//			int mw = Mouse.getDWheel() / 120;
//
//			if (mx > 0 && mx < world.getMap().width() * world.getMap().scale() &&
//					my > 0 && mx < world.getMap().height() * world.getMap().scale() ) {
//
//				selectedTile = world.getMap().tileAt(mx, my);
//
//			}
//
//			if (selectedTile != null) {
//
//				if (MouseHelper.click(0)) {
//					if( selectedTile instanceof SpaceTile ) {
//						SpaceTile selected = (SpaceTile)selectedTile;
//						if (selected.getObject() == null) {
//							new TileObjectTorch(selected);
//						}
//					}
//					if( selectedTile instanceof BlockTile ) {
//						BlockTile selected = (BlockTile)selectedTile;
//						selected.destroy();
//					}
//				}
//
//				if (MouseHelper.click(1)) {
//					if( selectedTile instanceof SpaceTile ) {
//						SpaceTile selected = (SpaceTile)selectedTile;
//						if (selected.getObject() != null && selected.getObject().getType() == TileObjectType.TORCH) {
//							TileObjectTorch torch = (TileObjectTorch) selected.getObject();
//
//							torch.destroy();
//						}
//
//					}
//				}
//
//			}
//
//			world.update(delta);
//			camera.update(delta, world);
//		}

//		if (gamestate == game.Gamestate.DEAD) {
//			if(KeyboardHelper.pressed(Keyboard.KEY_SPACE)) {
//				changeGamestate(game.Gamestate.MAIN);
//				world.player.resurrect();
//			}
//		}

	}

	public static void render(double delta) {
		// TODO use RenderWrapper
		renderer.clear();
//		lineRenderer.clear();


		// drawText implicitly contains
		// texture.bind()
		// renderer.begin()
		// renderer.end()
		// Each texture to be rendered needs to be surrounded by these calls?
		renderer.drawTextCenterX("B I G   T I M E", scr_x/2, scr_y - 50);

		for (Entity entity : tileEntities) {
			entity.draw(renderer, delta);
		}

		for (Entity entity : micros) {
			entity.draw(renderer, delta);
		}

		player.draw(renderer, delta);

		player2.draw(renderer, delta);




//		if (gamestate == game.Gamestate.TITLE) {
//			UIHelper.drawStringCentered(scr_x/2, 200, "Press [Enter] to begin", Color.red);
//			UIHelper.drawStringCentered(scr_x/2, 220, "Press [Space] for Options", Color.red);
//			UIHelper.drawStringCentered(scr_x/2, 250, "v0.01", Color.red);
//		}

//		if (gamestate == game.Gamestate.MAIN || gamestate == game.Gamestate.PAUSED) {
//			/* Draw the world */
//			camera.draw(world);
//
//			/* Draw debug info */
//			Color.white.bind();
//			UIHelper.drawString(400, 10, "Map: " + world.getMap().name(), txtCol);
//			UIHelper.drawString(400, 25, world.getMap().width() + "x" + world.getMap().height() + "  scale: " + world.getMap().scale(),
//					                 txtCol);
//
//			UIHelper.drawString(400, 55, "Player Health: " + world.player.getHp(), txtCol);
//			UIHelper.drawString(400, 70, "Player State: " + world.player.getState().toString(), txtCol);
//			//UIHelper.drawString(400, 70, "Player speedFactor: " + world.player.speedFactor, txtCol);
//			UIHelper.drawString(400, 85, "Player velY: " + (world.player.getVel().getY() < 500 ? world.player.getVel().getY() : "MAX!"), txtCol);
//
//			UIHelper.drawString(400, 115, "Screen X: " + camera.bounds.getPos().getX() + ", Y: " + camera.bounds.getPos().getY(), txtCol);
//
//			UIHelper.drawString(400, 145, "timescale: " + timescale, txtCol);
//			//------
//
//			//Draw Tile Border
//			if( selectedTile != null) {
//				tileBorder.draw(new Vector2D(selectedTile.getLocx() * Tile.getScale(), selectedTile.getLocy() * Tile.getScale()).add(camera.offset()), new Color(0,255,0) );
//
//				UIHelper.drawString(400, 170, "Selected Tile Information: ", txtCol);
//				UIHelper.drawString(420, 185, "Location: " + selectedTile.getLocx() + ", " + selectedTile.getLocy(), txtCol);
//				UIHelper.drawString(420, 200, "Class: " + selectedTile.getClass().getName(), txtCol);
//
//				if( selectedTile instanceof SpaceTile ) {
//					SpaceTile tile = (SpaceTile)selectedTile;
//					//if( tile.object != null )
//						UIHelper.drawString(420, 215, "Contains: " + (tile.getObject() == null ? "Null" : tile.getObject().getClass().getName()), txtCol);
//				}
//
//
//				UIHelper.drawString(420, 230, "lightLevel: " + selectedTile.getLightLevel(), txtCol);
//			}
//		}
//
//		if (gamestate == game.Gamestate.DEAD) {
//			/* Draw the world */
//			camera.draw(world);
//
//			UIHelper.drawStringCentered(scr_x/2, 200, "You have died. Press [Space] to continue", Color.red);
//		}
//
//		if (gamestate == game.Gamestate.PAUSED) {
//			//GLHelper.rectfill(0, 0, scr_x, scr_y, new Color(0,0,0,200));
//
//			for (GUIWindow win : pauseWindows) {
//				if (win.isActive())
//					win.draw();
//			}
//		}

	}

	public static void main(String[] argv){
		Game game = new Game();
		game.start();

		return;
	}

	public static void playerDead() {
		changeGamestate(Gamestate.DEAD);
	}

//	public static String executeMenu(AMenu menu) {
//		String response = null;
//
//		while (response == null) {
//			KeyboardHelper.refresh();
//			response = menu.update();
//
//			double delta = getDelta();
//
//			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
//			render();
//			menu.render();
//
//			Display.update();
//			Display.sync(60);
//
//			if (Display.isCloseRequested()) {
//				Display.destroy();
//				System.exit(0);
//			}
//		}
//
//		return response;
//	}


	public static void changeGamestate(Gamestate newstate) {
		prevgamestate = gamestate;
		gamestate = newstate;
	}


	public void exit() {
		vao.delete();
		vbo.delete();
		ebo.delete();
		texture.delete();
		vertexShader.delete();
		fragmentShader.delete();
		program.delete();
	}
}
