package entities;

import geom.Vector2D;

public enum Direction {
    LEFT (-1),
    RIGHT (1);

    private final int xdir;

    Direction(int xdir) {
        this.xdir = xdir;
    }

    public Vector2D vec() {
        return new Vector2D(this.xdir, 0);
    }

    public int val() {
        return this.xdir;
    }

    public Vector2D point(Vector2D base) {
        return new Vector2D(base.getX() * this.xdir, base.getY());
    }

    public Direction flip() {
        switch(this) {
            case LEFT: return RIGHT;
            case RIGHT: return LEFT;
        }
        return null;
    }

}
