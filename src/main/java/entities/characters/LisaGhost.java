package entities.characters;

import animation.Animation;
import controllers.IController;
import entities.EntitySize;
import entities.physics.PlayerEntity;
import geom.Vector2D;

import java.util.HashMap;

public class LisaGhost extends PlayerEntity {
    public LisaGhost(IController controller, Vector2D pos) {
        super(pos);
        super.init(controller, pos);
    }

    public void init(IController controller, Vector2D pos) {
        super.init(controller, pos);
    }

    @Override
    protected void loadAnimations() {
        try {
            this.animationSizeSet = new HashMap<>();
            this.animationSizeSet.put(
                    EntitySize.S0,
                    Animation.loadSSA("LisaSprites_S0.ssa")
            );
            this.animationSizeSet.put(
                    EntitySize.S1,
                    Animation.loadSSA("LisaSprites.ssa")
            );
            this.animations = animationSizeSet.get(this.size);
        } catch (Exception e) {
            throw new RuntimeException("Could not load player animations", e);
        }
    }
}
