package entities.states;

public class PlayerState {


    private PostureState postureState;
    private MovementState movementState;
    private ActionState actionState;
    private HealthState healthState;

    public boolean isAlive;
    public boolean hasControl;

    public PlayerState() {
        this.isAlive = true;
        this.hasControl = true;
        this.postureState = PostureState.UPRIGHT;
        this.movementState = MovementState.IDLE;
        this.actionState = ActionState.IDLE;
        this.healthState = HealthState.OKAY;
    }

    public void setState(PostureState state) {this.postureState = state;}
    public void setState(MovementState state) {this.movementState = state;}
    public void setState(ActionState state) {this.actionState = state;}
    public void setState(HealthState state) {this.healthState = state;}

    public boolean isStanding() {
        return (
            postureState == PostureState.UPRIGHT &&
            movementState == MovementState.IDLE
        );
    }

    public PostureState getPostureState() {
        return postureState;
    }

    public MovementState getMovementState() {
        return movementState;
    }

    public ActionState getActionState() {
        return actionState;
    }

    public HealthState getHealthState() {
        return healthState;
    }
}


