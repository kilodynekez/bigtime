package entities.states;

public enum ActionState {
    IDLE,
    GRABBING,
    EATING,
    HOLDING,
    STOMPING
}
