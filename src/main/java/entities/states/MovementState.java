package entities.states;

public enum MovementState {
    IDLE,
    MOVING,
    STUNNED,
    SHRINKING,
    SQUISHING,
    GROWING;
}
