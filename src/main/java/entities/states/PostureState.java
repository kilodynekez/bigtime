package entities.states;

public enum PostureState {
    UPRIGHT,
    CROUCHING,
    AIRBORNE,
    CLIMBING
}
