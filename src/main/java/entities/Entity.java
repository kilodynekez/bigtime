package entities;

import animation.Animation;
import animation.Frame;
import events.AttackEvent;
import events.StompEvent;
import geom.Vector2D;
import graphic.RenderWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Entity {
    /**
     * The x-y position of the entity, as the bottom-left point in OpenGL coordinates
     */
    protected Vector2D pos;

    protected Map<EntitySize, Map<String, Animation>> animationSizeSet;
    protected Map<String, Animation> animations;
    protected String currentAnimation;
    protected Direction dir;
    protected EntitySize size;

    public Entity(Vector2D pos) {
        this.pos = pos;
    }

    public EntitySize getSize() {
        return size;
    }

    public Vector2D getPos() {
        return pos;
    }

    protected void loadAnimations() {
        throw new RuntimeException("loadAnimations must be overridden");
    }


    public void update(double delta) {

    }

    public void draw(RenderWrapper renderer, double delta) {

    }

    public Frame getCurrentFrame() {
        return this.animations.get(currentAnimation).getCurrentFrame();
    }



    public BoundingBox getHitbox() {
        return getCurrentFrame().getHitbox();
    }
    public BoundingBox getHurtbox() {
        return getCurrentFrame().getHurtbox();
    }


    protected Animation switchAnimation(String animationName) {
        if (!animationName.equals(currentAnimation)) {
            currentAnimation = animationName;
            animations.get(currentAnimation).restart();
        }
        Animation animation = animations.get(currentAnimation);

        // update frame and hitboxes too
        animation.getCurrentFrame().update(0, pos, dir);

        return animation;
    }

    protected void switchAnimationSet(EntitySize size) {
        this.animations = this.animationSizeSet.get(size);
    }

    protected Animation getAnimation() {
        return animations.get(currentAnimation);
    }

    public void setPos(Vector2D newPos) {
        this.pos = newPos;
    }



    public List<AttackEvent> initiatedAttackEvents() {
        // TODO extract a lot of PlayerEntity logic to here
        throw new RuntimeException("Not Implemented in base class");
    }

    public void cancelAttackEvent(AttackEvent event) {
        // TODO extract a lot of PlayerEntity logic to here
        throw new RuntimeException("Not Implemented in base class");
    }

    public void affectByEvent(AttackEvent event) {
        // TODO extract a lot of PlayerEntity logic to here
        throw new RuntimeException("Not Implemented in base class");
    }

    public void affectByEvent(StompEvent event) {
        // TODO extract a lot of PlayerEntity logic to here
        throw new RuntimeException("Not Implemented in base class");
    }
}
