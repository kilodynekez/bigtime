package entities.physics;

import animation.Animation;
import entities.EntitySize;
import entities.states.MovementState;
import entities.states.PostureState;
import events.StompEvent;
import geom.Vector2D;
import graphic.RenderWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static entities.Direction.RIGHT;

public class MicroEntity extends PhysicsEntity {
    List<String> genericMicroSSAs = new ArrayList<> (
        Arrays.asList(
            "MicroSprites-Kez.ssa",
            "MicroSprites-Sean.ssa"
        )
    );

    static Random rand = new Random();
    private double walkTimer;

    public enum MicroState {IDLE, WALKING, SQUISHING, SQUISHED}

    public PostureState pState;

    MicroState state;
    private boolean alive;

    // 2 pixels per 125ms
    double speedGround =  (1000d / 100d) * 2;

    String ssaFile;

    public MicroEntity(Vector2D pos) {
        super(pos);
        init(pos);
    }
    public MicroEntity(Vector2D pos, String ssaFile) {
        super(pos);
        this.ssaFile = ssaFile;
        init(pos);
    }

    public void init(Vector2D pos) {
        // need graphics, controller

        this.alive = true;
        this.pos = pos;
        this.size = EntitySize.S0;

        if (ssaFile == null) {
            // use random SSA
            ssaFile = genericMicroSSAs.get(rand.nextInt(genericMicroSSAs.size()));
        }

        loadAnimations();

        currentAnimation = "Idle";
        this.state = MicroState.IDLE;
        this.pState = PostureState.UPRIGHT;
        this.dir = RIGHT;

        this.walkTimer = 0;
    }

    @Override
    protected void loadAnimations() {
        try {
            this.animations = Animation.loadSSA(this.ssaFile);
        } catch (Exception e) {
            throw new RuntimeException("Could not load micro animations", e);
        }
    }

    @Override
    public void update(double delta) {
        // update velocity from gravity
        super.update(delta);

        // assume airborne until otherwise interacted on
//        setPState(PostureState.AIRBORNE);

        // If the micro is done with their current random walk, generate another
        if (state == MicroState.IDLE || state == MicroState.WALKING) {
            if (walkTimer <= 0 ) {
                walkTimer = 1 + rand.nextInt(3);  // between 1 and 4 seconds

                // 70% chance to flip directions
                if (rand.nextFloat() <= 0.7f) {
                    this.dir = this.dir.flip();
                }

                if (rand.nextFloat() <= 0.2f) { // 20% chance to stand, 80% to move
                    changeState(MicroState.IDLE);
                } else {
                    changeState(MicroState.WALKING);
                }
            }

            walkTimer -= delta;
        }

        // Keep the old position before any modifications are made
        Vector2D oldPos = this.pos;

        Vector2D desiredMovt = new Vector2D(0,0);
        if (state == MicroState.WALKING) {
            desiredMovt = dir.point(new Vector2D(speedGround * delta, 0f));
        }

        desiredMovt = desiredMovt.add(this.velocity);

        // TODO collision stuff here, calculate actual movement
        // --- err, I guess this happens in second update function (after interactions?)
        Vector2D actualMovt = desiredMovt;

        // Calculate new position
        Vector2D newPos = oldPos.add(actualMovt);

        this.pos = newPos;

        // Update frame and hitbox
        animations.get(currentAnimation).update(delta, this.pos, this.dir);
    }


    @Override
    public void draw(RenderWrapper renderer, double delta) {
        // draw self
        animations.get(currentAnimation).render(renderer, delta, this.pos, this.dir);
    }

    @Override
    public void affectByEvent(StompEvent event) {
        // Micros always get squished by stomps.
        // Go into squishing state, and then squished.
//        if (this.alive){
            squish();
//        }
    }

    private void squish() {
        this.alive = false;
        changeState(MicroState.SQUISHING);
    }

    private String determineAnimation() {
        if (pState == PostureState.AIRBORNE) {
            return "Falling";
        }
        if (pState == PostureState.UPRIGHT) {
            switch(state) {
                case IDLE -> {
                    return "Idle";
                }
                case WALKING -> {
                    return "Walk";
                }
                case SQUISHING -> {
                    return "Squishing";
                }
                case SQUISHED -> {
                    return "Squished";
                }
            }
        }
        return "";
    }

    private void changeState(MicroState newState) {
        switch(newState) {
            case IDLE -> {
            }
            case WALKING -> {
            }
            case SQUISHING -> {
                getAnimation().whenComplete(
                        () -> changeState(MicroState.SQUISHED)
                );
            }
            case SQUISHED -> {
            }
        }
        this.state = newState;
        switchAnimation(determineAnimation());
    }

    public void setPState(PostureState pState) {
        switch (pState) {
            case UPRIGHT -> {
                this.setVelocity(new Vector2D(0, 0));
            }
        }
        this.pState = pState;
        switchAnimation(determineAnimation());
    }
    @Override
    public void collideWithFloor(){
        setPState(PostureState.UPRIGHT);
    }
    @Override
    public void enterFreefall(){
        setPState(PostureState.AIRBORNE);
    }
}
