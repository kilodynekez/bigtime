package entities.physics;

import controllers.Controls;
import controllers.IController;
import entities.Direction;
import entities.Entity;
import entities.EntitySize;
import entities.states.*;
import events.AttackEvent;
import events.StompEvent;
import geom.Vector2D;
import graphic.RenderWrapper;

import java.util.ArrayList;
import java.util.List;

import static entities.Direction.RIGHT;

public class PlayerEntity extends PhysicsEntity {
    private double totalMovementOffset;
    private List<AttackEvent> attackEvents;

    private enum MovementStyle {ALIGNED, FREE}
    private int movementAlignment = 5;


    IController controller;

    PlayerState state;

    MovementStyle movtStyle;

//    double speedGround = 40f;  // pixels per second

    // 5 pixels per 125ms
    double baseSpeedGround =  (1000d / 100d) * 5;
    double speedGround =  baseSpeedGround;

    public PlayerEntity(Vector2D pos) {
        super(pos);
    }

    public void init(IController controller, Vector2D pos) {
        // need graphics, controller

        this.pos = pos;
        this.controller = controller;
        this.size = EntitySize.S1;

        this.movtStyle = MovementStyle.FREE;

        loadAnimations();

        currentAnimation = "Idle";
        this.attackEvents = new ArrayList<>();
        this.state = new PlayerState();
        this.dir = RIGHT;

    }

    private void changeMovementState(MovementState newState){
        if (!newState.equals(movementState())) {
            PlayerState curState = this.state;
            state.setState(newState);
            // moving out of old states...
//            switch(curState) {
//                case STOMP -> {
//                    // TODO this will all have to be redone once
//                    // we have a proper animation and state change system
//                    this.attackEvents = new ArrayList<>();
//                }
//            }

            // and into new states...
            switch(newState) {
                case MOVING -> {
//                    System.out.format("Resetting total movement\n");
                    this.totalMovementOffset = 0;
//                    switchAnimation("WalkInPlace");
                }
                case IDLE -> {
//                    switchAnimation("Idle");
                }
                case SHRINKING -> {
//                    switchAnimation("Shrinking");
                    String newAnim = determineAnimation();
                    System.out.format("On shrink start, animation is: " + newAnim + "\n") ;
                    takeControlAway();
                    switchAnimation(determineAnimation()).whenComplete( () -> {
                                changeMovementState(MovementState.IDLE);
                                this.size = this.size.shrink();

                                this.speedGround = baseSpeedGround * this.size.speedFactor();

                                // Change animation set to next size
                                switchAnimationSet(this.size);
//                                switchAnimation("Idle");

                                String nextAnim = determineAnimation();
                                System.out.format("After shrink finished, animation is: " + nextAnim + "\n") ;

                                switchAnimation(determineAnimation());



                                giveControlBack();
                            }
                    );
                }
                case SQUISHING -> {
//                    switchAnimation("Squishing");
                    this.state.hasControl = false;
                    switchAnimation(determineAnimation()).whenComplete(
                            () -> changeHealthState(HealthState.SQUISHED)
                    );
                }
            }

        }
        switchAnimation(determineAnimation());

    }

    private void changeActionState(ActionState newState){
//        if (!newState.equals(this.state.actionState)) {
            ActionState curState = actionState();
            state.setState(newState);

//            // moving out of old states...
//            switch(curState) {
//                case STOMPING -> {
//                }
//            }

            // and into new states...
            switch(newState) {
                case STOMPING -> {
                    changeMovementState(MovementState.IDLE);
                    this.attackEvents.add(new StompEvent(this));
                    // Can't move until the animation is complete
                    takeControlAway();
                    switchAnimation("Stomp").whenComplete(() -> {
                        giveControlBack();

                        // TODO Resolve the attackEvents better??
                        this.attackEvents = new ArrayList<>();

                        changeActionState(ActionState.IDLE);
                    });
                    getAnimation().restart();
                }
//                case IDLE -> switchAnimation("Idle");
            }

//        }
        switchAnimation(determineAnimation());

    }

    private void changeHealthState(HealthState newState){
        if (!newState.equals(healthState())) {
            HealthState curState = healthState();
            state.setState(newState);

            // moving out of old states...
//            switch(curState) {
//                case STOMPING -> {
//                    this.attackEvents = new ArrayList<>();
//                }
//            }

            // and into new states...
            switch(newState) {
                case SQUISHED -> {
                    this.state.isAlive = false;
//                    switchAnimation("Squished");
                }
            }

        }

        switchAnimation(determineAnimation());

    }

    private void changePostureState(PostureState newState) {
        PostureState curstate = postureState();
        state.setState(newState);

        switch(newState) {
            case UPRIGHT -> {
                this.setVelocity(new Vector2D(0,0));
            }
        }
        switchAnimation(determineAnimation());
    }

//    private void changeState(HealthState newState){
//        if (!newState.equals(this.state.healthState)) {
//            HealthState curState = this.state.healthState;
//
//            // moving out of old states...
////            switch(curState) {
////                case STOMPING -> {
////                    this.attackEvents = new ArrayList<>();
////                }
////            }
//
//            // and into new states...
//            switch(newState) {
//                case STOMPING -> {
//                    this.attackEvents.add(new StompEvent(this));
//                    switchAnimation("Stomp");
//                }
//                case STANDING -> switchAnimation("Idle");
//                case SQUISHED -> {
//                    switchAnimation("Squished");
//                }
//            }
//
//            this.state = newState;
//        }
//
//    }


    @Override
    public void update(double delta) {
        // update velocity from gravity
        super.update(delta);

        // do stuff based on controls and other state

        if (state.isAlive) {
            if (state.hasControl) {
                MovementState newMovtState = MovementState.IDLE;

                if(controller.active(Controls.LEFT)) {
                    newMovtState = MovementState.MOVING;
                    this.dir = Direction.LEFT;
                }
                if(controller.active(Controls.RIGHT)) {
                    newMovtState = MovementState.MOVING;
                    this.dir = Direction.RIGHT;
                }

                changeMovementState(newMovtState);

                if (size.isGreaterThan(EntitySize.S0)) {
                    if(controller.justPressed(Controls.DOWN)) {
                        changeActionState(ActionState.STOMPING);
                    }
                }
            } else {
                if (actionState() == ActionState.STOMPING) {
                    if(controller.justPressed(Controls.DOWN)) {
                        changeActionState(ActionState.STOMPING);
                    }
                }
            }

//        if(controller.active(Controls.UP)) {
//            newMovtState = entities.states.PlayerState.STOMP_REEL;
//        }
//        if(controller.active(Controls.DOWN)) {
//            newMovtState = entities.states.PlayerState.STOMP;
//        }

        }

        if (controller.justPressed(Controls.SHRINKSELF)) {
            shrink();
        }

        if(controller.justPressed(Controls.ToggleMovt)) {
//            System.out.format("[Toggling movement]\n");
            if (this.movtStyle == MovementStyle.ALIGNED) {
                this.movtStyle = MovementStyle.FREE;
            } else {
                this.movtStyle = MovementStyle.ALIGNED;
            }
        }


        // After states have been confirmed, do special logic depending on the state



        // Keep the old position before any modifications are made
        Vector2D oldPos = this.pos;

        // Desired movement vector based on walk speed
        Vector2D desiredMovt =
                movementState() == MovementState.MOVING ?
                dir.point(new Vector2D(speedGround * delta, 0f)) :
                new Vector2D(0,0);

        // Add velocity from gravity (or other sources)
        desiredMovt = desiredMovt.add(this.velocity);

        // TODO collision stuff here, calculate actual movement
        // --- err, I guess this happens in second update function (after interactions?)
        Vector2D actualMovt = desiredMovt;

        // Calculate new position
        Vector2D newPos = oldPos.add(actualMovt);


        double xdiff = newPos.getX() - oldPos.getX();
        this.totalMovementOffset += xdiff;

//        System.out.format("Player X diff: %f -> %f (%d) ", xdiff, totalMovementOffset, Math.round(totalMovementOffset));

        // Figure out how much to actually draw the player based on the movement style
        if (this.movtStyle == MovementStyle.ALIGNED) {
            long alignedOffset = Math.round(this.totalMovementOffset);
            if (alignedOffset % movementAlignment == 0) {
                this.pos = this.pos.add(alignedOffset, 0f);
//                System.out.format("[Jitter update]");
                this.totalMovementOffset -= alignedOffset;
            }
        } else if (this.movtStyle == MovementStyle.FREE) {
            this.pos = newPos;
        }

//        System.out.format("\n");
        animations.get(currentAnimation).update(delta, this.pos, this.dir);

//        System.out.format("--[draw]-------------------------------\n");
    }


    @Override
    public List<AttackEvent> initiatedAttackEvents() {
        return this.attackEvents;
    }
    public void cancelAttackEvent(AttackEvent event) {
        this.attackEvents.remove(event);
        // TODO other consequences?  Like getting stunned, etc.
    }

    @Override
    public void affectByEvent(StompEvent event) {
//        if (size == EntitySize.S0) {
//            // TODO: if S0, would be automatic squish.
//            // Contests should only occur if ratio is 1/4
//
//            // Start a contest
//
//        }
        if (size == EntitySize.S0) {
            squish();
        }
    }

    public void shrink() {
        if (this.size == EntitySize.S0) {
            // TODO special death condition?
        } else {
            // Shrink player smaller
            changeMovementState(MovementState.SHRINKING);
        }
    }
    private void squish() {
        this.state.isAlive = false;
        changeMovementState(MovementState.SQUISHING);
    }

    private void takeControlAway() {
        this.state.hasControl = false;
    }
    private void giveControlBack() {
        this.state.hasControl = true;
    }

    @Override
    public void collideWithFloor(){
        changePostureState(PostureState.UPRIGHT);
    }
    @Override
    public void enterFreefall(){
        changePostureState(PostureState.AIRBORNE);
    }

    private String determineAnimation() {
        if (healthState() == HealthState.SQUISHED) {
            return "Squished";
        }
        if (postureState() == PostureState.AIRBORNE) {
            return "Falling";
        }
        if (postureState() == PostureState.UPRIGHT) {
            switch(actionState()) {
                case IDLE -> {
                    switch(movementState()) {
                        case IDLE -> {
                            return "Idle";
                        }
                        case MOVING -> {
                            return "WalkInPlace";
                        }
                        case SHRINKING -> {
                            return "Shrinking";
                        }
                        case SQUISHING -> {
                            // TODO BUG If this state is set this way, then we don't perform
                            // a callback to set the related healthstate and the animation
                            // keeps looping forever 
                            return "Squishing";
                        }
                    }
                }
                case STOMPING -> {
                    return "Stomp";
                }
            }
        }
        return "";
    }

    @Override
    public void draw(RenderWrapper renderer, double delta) {
        // draw self
        animations.get(currentAnimation).render(renderer, delta, this.pos, this.dir);
    }

    public MovementState movementState() { return this.state.getMovementState();}
    public PostureState postureState() { return this.state.getPostureState();}
    public ActionState actionState() { return this.state.getActionState();}
    public HealthState healthState() { return this.state.getHealthState();}
}
