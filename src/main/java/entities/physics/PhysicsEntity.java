package entities.physics;

import animation.Animation;
import animation.Frame;
import entities.BoundingBox;
import entities.Direction;
import entities.Entity;
import entities.EntitySize;
import events.AttackEvent;
import events.StompEvent;
import geom.Vector2D;
import graphic.RenderWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PhysicsEntity extends Entity {
    public PhysicsEntity(Vector2D pos)  {
        super(pos);
    }

    public EntitySize getSize() {
        return size;
    }

    public Vector2D getPos() {
        return pos;
    }

    /** Measured in pixels per frame per frame **/
    protected Vector2D gravityAcceleration = new Vector2D(0, -0.2f);
    protected float terminalVelocity = -10;

    protected List<Vector2D> accelerationVectors =
            new ArrayList<>(
                    Arrays.asList(
                            gravityAcceleration
                    )
            );

    protected Vector2D velocity = new Vector2D(0,0);

    public void update(double delta) {
        for (Vector2D acl : accelerationVectors) {
            velocity = velocity.add(acl);
        }

        if (velocity.getY() < terminalVelocity)  {
            velocity = new Vector2D(velocity.getX(), terminalVelocity);
        }
    }

    public void setVelocity(Vector2D newVel) {
        this.velocity = newVel;
    }

    public Vector2D getVelocity() {
        return this.velocity;
    }

    public void setPos(Vector2D newPos) {
        this.pos = newPos;
    }

    public void collideWithFloor() {
        throw new RuntimeException("collideWithFloor not implemented");
    };
    public void enterFreefall() {
        throw new RuntimeException("enterFreefall not implemented");
    };


}
