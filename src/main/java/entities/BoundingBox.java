package entities;

import geom.Rectangle2D;
import geom.Vector2D;
import graphic.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A generic class from which Hitbox and Hurtbox can inherit
 */
public class BoundingBox {

    private Vector2D flipAxis;

    /**
     * Returns a copy of all included rectangles, with the offset of the bounding box position
     */
    public List<Rectangle2D> getRects() {
        ArrayList<Rectangle2D> result = new ArrayList<Rectangle2D>();
        for (Rectangle2D rect : this.rects) {
            result.add(processReturnedRect(rect)) ;
        }
        return result;
    }

    /**
     * Returns a copy of the bounding rectangle, with the offset of the bounding box position
     */
    public Rectangle2D getBounds() {
        return processReturnedRect(this.bounds);
    }

    private Rectangle2D processReturnedRect(Rectangle2D rect) {
        Rectangle2D newRect = rect.add(this.pos);
        if (this.dir == Direction.LEFT) {
            newRect = newRect.flipAboutX(this.flipAxis);
        }
        return newRect;
    }

    /**
     * The list of rectangles that make up the bounds
     */
    List<Rectangle2D> rects;

    /**
     * A single bounding rectangle that surrounds all the rectangles in rects
     */
    Rectangle2D bounds;

    /**
     * Order in which the rectangles in rects should be visited if traversing from
     * left to right
     */
    List<Integer> orderLR;
    /**
     * Order in which the rectangles in rects should be visited if traversing from
     * right to left
     */
    List<Integer> orderRL;

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    Direction dir;

    public Color color;

    Vector2D pos;

    public BoundingBox(Map<String, Object> bbData) {
        initData(bbData);
        color = new Color(0f,1f,0f);
    }
    public BoundingBox(Map<String, Object> bbData, Color c) {
        initData(bbData);
        color = c;
    }
    public BoundingBox(BoundingBox other){
        this.pos = other.pos;
        this.dir = other.dir;
        this.rects = other.rects;
        this.flipAxis = other.flipAxis;
        this.color = other.color;
        this.bounds = other.bounds;
        this.orderLR = other.orderLR;
        this.orderRL = other.orderRL;
    }
    public BoundingBox copy() {
        return new BoundingBox(this);
    }

    private void initData(Map<String, Object> bbData) {
        pos = new Vector2D(0,0);
        rects = new ArrayList<Rectangle2D>();
        addRects((List<Map<String, Integer>>) bbData.get("rects"));
        setBounds((Map<String, Integer>) bbData.get("bounds"));
        setOrderLR((List<Integer>) bbData.get("orderLR"));
        setOrderRL((List<Integer>) bbData.get("orderRL"));
        dir = Direction.RIGHT;
        flipAxis = new Vector2D(0,0);
    }

    private void addRects(List<Map<String,Integer>> rectList) {
        for (Map<String,Integer> rData : rectList) {
            addRect(rData);
        }
    }

    private void addRect(Map<String, Integer> rData) {
        rects.add(
            new Rectangle2D(
                    rData.get("x"),
                    rData.get("y"),
                    rData.get("w"),
                    rData.get("h")
            )
        );
    }

    private void setBounds(Map<String,Integer> rData) {
       bounds =  new Rectangle2D(
                        rData.get("x"),
                        rData.get("y"),
                        rData.get("w"),
                        rData.get("h")
                );
    }

    private void setOrderLR(List<Integer> order) {
        this.orderLR = order;
    }
    private void setOrderRL(List<Integer> order) {
        this.orderRL = order;
    }

    public void updatePosition(Vector2D pos) {
        this.pos = pos;
    };

    public void updateFlipAxis(Vector2D axis) {
        this.flipAxis = axis;
    }

    public boolean overlaps(BoundingBox other) {
        if (other == null) { return false; }

        // First, see if the bounding boxes overlap.  If they don't,
        // then we can exit early
        if (this.getBounds().overlaps(other.getBounds())) {

            // If so, then check all the inner rects of the
            // two bounding boxes against each other
            // TODO use orderLR and orderRL to make less comparisons
            for (Rectangle2D rect : this.getRects()) {
                for (Rectangle2D otherRect : other.getRects()) {
                    if (rect.overlaps(otherRect)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
