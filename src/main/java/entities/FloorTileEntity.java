package entities;

import animation.Animation;
import geom.Vector2D;
import graphic.RenderWrapper;

public class FloorTileEntity extends Entity {

    public enum TileEntityType  {
        OUTER_TOP_LEFT,
        OUTER_TOP,
        OUTER_TOP_RIGHT
        // etc...
    };

    protected TileEntityType tileType;

    public FloorTileEntity(Vector2D pos, TileEntityType tileType) {
        super(pos);
        this.pos = pos;
        this.tileType = tileType;

        loadAnimations();
        currentAnimation = "Idle";

        // TODO use the tileType to determine which sprite in the sheet to grab
        // TODO maybe use it in collision logic too
    }

    public void update(double delta) {
        // Update frame and hitbox
        animations.get(currentAnimation).update(delta, this.pos, this.dir);
    }

    public void draw(RenderWrapper renderer, double delta) {
        animations.get(currentAnimation).render(renderer, delta, this.pos, this.dir);
    }

    @Override
    protected void loadAnimations() {
        try {
            this.animations = Animation.loadSSA("FloorTile.ssa");
        } catch (Exception e) {
            throw new RuntimeException("Could not load floor animations", e);
        }
    }
}
