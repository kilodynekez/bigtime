package entities;

public enum EntitySize {
    S0,  // 8px     Shrunk
    S1,  // 64px    Base
    S2,  // 128 px
    S3,  // 256 px
    S4,  // 512 px
    S5;  // 4098 px     Fatality

    public int height() {
        switch (this) {
            case S0: return 8;
            case S1: return 64;
            case S2: return 128;
            case S3: return 256;
            case S4: return 512;
            case S5: return 4098;
        }
        return 0;
    }

    public static double ratioBetween(EntitySize a, EntitySize b) {
        return ((double) a.height()) / b.height();
    }

    public double ratio(EntitySize other) {
        return ratioBetween(this, other);
    }

    public EntitySize shrink() {
        switch (this) {
            case S0: return S0;
            case S1: return S0;
            case S2: return S1;
            case S3: return S2;
            case S4: return S3;
            case S5: return S4;
        }
        return null;
    }
    public EntitySize grow() {
        switch (this) {
            case S0: return S1;
            case S1: return S2;
            case S2: return S3;
            case S3: return S4;
            case S4: return S5;
            case S5: return S5;
        }
        return null;
    }

    public double speedFactor() {
        switch (this) {
            case S0: return 0.60;
            case S1: return 1;
            case S2: return 1.2;
            case S3: return 1.4;
            case S4: return 1.6;
            case S5: return 2;
        }
        return 0;
    }

    public boolean isGreaterThan(EntitySize other) {
        return this.compareTo(other) > 0;
    }
    public boolean isLessThan(EntitySize other) {
        return this.compareTo(other) < 0;
    }
}
