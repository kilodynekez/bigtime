package animation;

public enum AnimDirection {
    FORWARD, BACKWARD, PINGPONG;


    public static AnimDirection fromString(String str) {
        switch(str.toLowerCase()) {
            case "forward":
                return AnimDirection.FORWARD;
            case "backward":
                return AnimDirection.BACKWARD;
            case "pingpong":
                return AnimDirection.PINGPONG;
        }
        return null;
    }
}
