package animation;

import com.fasterxml.jackson.databind.ObjectMapper;
import entities.Direction;
import geom.Vector2D;
import graphic.RenderWrapper;
import graphic.Texture;
import resources.Resources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Animation {
    private static Map<String, Texture> textures = new HashMap<>();



    private String name;
    private int numFrames;
    private AnimDirection direction;
    private int currentDir = 1;
    private List<Frame> frames;
    private Texture sheet;

    private double elapsedSecondsTotal = 0;
    private double elapsedSecondsOnFrame = 0;
    private int currentFrame = 0;
    private Runnable whenCompleteCallback;


    public Animation(Map<String, Object> ssaData) {

        // Load or retrieve the Texture for this animation's sprite sheet
        String sheetName = (String) ssaData.get("sheet");
        this.sheet = getTexture(sheetName);

        this.name = (String) ssaData.get("name");
        this.numFrames = (int) ssaData.get("frameCount");
        this.direction = AnimDirection.fromString((String) ssaData.get("direction"));

        // Make Frame objects
        this.frames = new ArrayList<>();
        Map<String, Object> frames = (Map<String, Object>) ssaData.get("frames");
        for(int i = 0; i < this.numFrames; i++) {
            String iStr = Integer.toString(i);

            Map<String, Object> fData = (Map<String, Object>) frames.get(iStr);

            Frame frame = new Frame(fData, sheet);

            this.frames.add(frame);

        }

    }

    public void restart() {
        this.elapsedSecondsTotal = 0;
        setFrame(0, 0);
    }

    public void setFrame(int idx, double elapsedOverflow) {
        this.currentFrame = idx;
        this.elapsedSecondsOnFrame = elapsedOverflow;
    }

    public Frame getCurrentFrame() {
       return this.frames.get(currentFrame);
    }

    public void advanceFrame(double elapsedOverflow) {

        if (currentFrame < frames.size() - 1) {
            if (currentFrame == 0 && direction == AnimDirection.PINGPONG) {
                currentDir = 1;  // reset direction forward
            }
            // In most cases, just advance the frame by 1
            setFrame(currentFrame+currentDir, elapsedOverflow);
        } else {
            // If we're at the end, see if we have a whenComplete callback.
            // If so, call it.
            if (whenCompleteCallback != null) {
                whenCompleteCallback.run();
            } else {
                // Otherwise, do stuff based on our direction
                switch (direction) {
                    case FORWARD:
                        setFrame(0, elapsedOverflow);
                        break;
                    case BACKWARD:
                        throw new RuntimeException("Backwards animations not implemented");
                    case PINGPONG:
                        currentDir = -1;  // set direction backward
                        setFrame(currentFrame-1, elapsedOverflow);
                        break;
                }
            }

        }
    }
    public void update(double delta, Vector2D pos, Direction dir)  {
        elapsedSecondsTotal += delta;
        elapsedSecondsOnFrame += delta;

        // Determine if we need to advance the frame, and update if so
        double frameDurSeconds = frames.get(currentFrame).getDurationSec();
//        System.out.format("Animation %s: %f | %f ", this.name, elapsedSecondsOnFrame, frameDurSeconds);
        if (elapsedSecondsOnFrame >= frameDurSeconds) {
//            System.out.format("ADVANCE");
            double elapsedOverflow = elapsedSecondsOnFrame - frameDurSeconds;
            advanceFrame(elapsedOverflow);
        }
//        System.out.format("\n");

        // Adjust the position of the bounding boxes for the current frame
        this.frames.get(currentFrame).update(delta, pos, dir);
    }

    public void render(RenderWrapper renderer, double delta, Vector2D pos, Direction dir) {
        this.frames.get(currentFrame).render(renderer, pos, dir);
    }

    public static Texture getTexture(String textureName) {
        Texture texture = textures.getOrDefault(textureName, null);

        if (texture == null) {
            texture = Texture.loadTexture(Path.of(Resources.SPRITE_ROOT, textureName));
            textures.put(textureName, texture);
        }

        return texture;
    }

    public static Map<String, Animation> loadSSA(String ssaFile) throws IOException {
        String json = Files.readString(Path.of(Resources.SPRITE_ROOT, ssaFile));

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(json, Map.class);

        Map<String, Animation> animations = new HashMap<>();

        // Create an Animation object for each described Animation
        for (String animationName : map.keySet()) {
            Map<String, Object> ssaData = (Map<String, Object>) map.get(animationName);

            animations.put(animationName, new Animation(ssaData));
        }


        return animations;

    }

    public void whenComplete(Runnable callback) {
        this.whenCompleteCallback = callback;
    }
}
