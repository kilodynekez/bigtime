package animation;

import entities.BoundingBox;
import entities.Direction;
import geom.Rectangle2D;
import geom.Vector2D;
import graphic.Color;
import graphic.RenderWrapper;
import graphic.Renderer;
import graphic.Texture;

import java.util.*;

public class Frame {

    List<FrameLayer> layers = new ArrayList<>();
    Texture sheet;
    int durationMs;
    double durationSec;

    Vector2D registrationOffset;

    public BoundingBox getHitbox() {
        return hitbox;
    }
    public boolean hasHitbox() {return this.hitbox != null; }

    public BoundingBox getHurtbox() {
        return hurtbox;
    }
    public boolean hasHurtbox() {return this.hurtbox != null; }

    BoundingBox hitbox;
    BoundingBox hurtbox;

    public Frame(Map<String, Object> fData, Texture sheet) {
        this.sheet = sheet;

        // Parse each graphic layer in the frame
        LinkedHashMap<String, Object> graphicLayers = (LinkedHashMap<String, Object>) fData.get("graphicLayers");
        for (Map.Entry<String, Object> entry : graphicLayers.entrySet()) {
            String layerName = entry.getKey();
            Map<String, Object> lData = (Map<String, Object>) entry.getValue();

            FrameLayer layer = new FrameLayer(layerName, lData, sheet);

            // TODO  check if different layers have different durations or registrationOffsets
            // (They shouldn't, these should be the same for the entire frame)
            // Also, get this data from the frame layer in a consistent way
            durationMs = (int) lData.get("duration");
            durationSec = durationMs / 1000d;
            registrationOffset = layer.registrationOffset;

            this.layers.add(layer);
        }

        Map<String, Object> metaLayers = (Map<String, Object>) fData.get("metaLayers");

        // Parse hitbox data
        Map<String, Object> hitboxData = (Map<String, Object>) metaLayers.get("Hitbox");
        if (hitboxData != null) {
            hitbox = new BoundingBox(hitboxData);
        }
        // Parse hurtbox data
        Map<String, Object> hurtboxData = (Map<String, Object>) metaLayers.get("Hurtbox");
        if (hurtboxData != null) {
            hurtbox = new BoundingBox(hurtboxData, new Color(1f,0,0));
        }
    }

    public double getDurationSec() { return durationSec; }

    public void update(double delta, Vector2D pos, Direction dir)  {
        if (hitbox != null) {
            this.hitbox.updatePosition(pos.sub(registrationOffset));
            this.hitbox.updateFlipAxis(pos);
            this.hitbox.setDir(dir);
        }
        if (hurtbox != null) {
            this.hurtbox.updatePosition(pos.sub(registrationOffset));
            this.hurtbox.updateFlipAxis(pos);
            this.hurtbox.setDir(dir);
        }
    }

    public void render(RenderWrapper renderer, Vector2D pos, Direction dir) {
        // Render the textures for all graphic layers
        // Begin and end the texture renderer here, since we will always
        // use the same texture (sprite sheet) for all the frames
        // and layers in a given animation, so we can bind it
        // once here.
        this.sheet.bind();
        renderer.textureRenderer.begin();
        for(FrameLayer layer : this.layers) {
            layer.render(renderer, pos, dir)      ;
        }
        renderer.textureRenderer.end();

        if (renderer.debug){

            // Draw hitbox, if any
            if (hitbox != null) {
//                hitbox.updatePosition(pos.sub(registrationOffset));
                // draw bounding box
                renderer.lineRenderer.begin();
                renderer.lineRenderer.drawRect(
                        hitbox.getBounds(),
                        new Color(0,1f,1f)
                );
                renderer.lineRenderer.end();
                // draw individual rects
                for (Rectangle2D r : hitbox.getRects()) {
                    renderer.lineRenderer.begin();
                    renderer.lineRenderer.drawRect(
                            r,
                            hitbox.color
                    );
                    renderer.lineRenderer.end();
                }
            }

            // Draw hurtbox if any
            if (hurtbox != null) {
//                hurtbox.updatePosition(pos.sub(registrationOffset));
                // draw individual rects
                for (Rectangle2D r : hurtbox.getRects()) {
                    renderer.lineRenderer.begin();
                    renderer.lineRenderer.drawRect(
                            r,
                            hurtbox.color
                    );
                    renderer.lineRenderer.end();
                }
            }

        }
    }

    private class FrameLayer {
        String name;
        Vector2D texturePos;
        Vector2D size;
        Vector2D registrationOffset;
        int durationMs;
        Texture sheet;



        public FrameLayer(String name, Map<String, Object> lData, Texture sheet) {
            this.name = name;
            this.sheet = sheet;

            Map<String, Integer> bounds = (Map<String, Integer>) lData.get("openGLOrigin");
            this.texturePos = new Vector2D(
                    bounds.get("x"), bounds.get("y")
            );

            Map<String, Integer> size = (Map<String, Integer>) lData.get("frame");
            this.size = new Vector2D(
                    size.get("w"), size.get("h")
            );

            Map<String, Integer> offset = (Map<String, Integer>) lData.get("registrationOffset");
            this.registrationOffset = new Vector2D(
                    offset.get("x"), offset.get("y")
            );

            this.durationMs = (int) lData.get("duration");

        }

        public void render(RenderWrapper renderer, Vector2D location, Direction dir) {
            Vector2D offsetLocation = location.sub(this.registrationOffset);
            Rectangle2D bounds = new Rectangle2D(offsetLocation, this.size);

            if (dir == Direction.LEFT){
                bounds = bounds.flipAboutX(location);
            }

            renderer.drawTextureRegion(
                    this.sheet,
                    (float) bounds.getX(),
                    (float) bounds.getY(),
                    (float) this.texturePos.getX(),
                    (float) this.texturePos.getY(),
                    (float) this.size.getX(),
                    (float) this.size.getY(),
                    dir == Direction.LEFT
            );

            if (renderer.debug){
                // Draw frame bounds
                renderer.lineRenderer.begin();
                renderer.lineRenderer.drawRect(
                        bounds,
                        new Color(1f,1f,0f));
                renderer.lineRenderer.end();
            }

        }

    }
}

