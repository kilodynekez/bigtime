package events;

import entities.BoundingBox;
import entities.Entity;
import jdk.jshell.spi.ExecutionControl;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeps track of an attack event over the course of multiple frames.
 * It responsible for keeping track of the attacker, any affected targets,
 * and any cancellations or other abnormal flow that may occur.
 */
public class AttackEvent {
    public Entity attacker;
    List<Entity> affectedTargets;

    public AttackEvent(Entity attacker) {
        this.attacker = attacker;
        this.affectedTargets = new ArrayList<>();
    }

    public List<Entity> determineAffectedTargetsThisFrame(List<Entity> potentialTargets) {
        List<Entity> affectedTargets = new ArrayList<>();

        // See if a hurtbox exists for this frame of the attack
        BoundingBox hurtbox = attacker.getHurtbox();
        if (hurtbox != null) {
            for (Entity target : potentialTargets) {
                // Determine if there is an overlap between the attaching player's hurtbox
                // and the target's hitbox
                if (hurtbox.overlaps(target.getHitbox())) {
                    // If so, add to list of affected targets
                    affectedTargets.add(target);
                }
            }
        }

        return affectedTargets;
    }

    public void apply(List<Entity> targets) {
        // TODO extract a lot of PlayerEntity logic to here
        throw new RuntimeException("Not Implemented in base class");
    }
}
