package events;

import entities.Entity;
import geom.Vector2D;

import java.util.List;

public class StompEvent extends AttackEvent {

    public StompEvent(Entity attacker) {
        super(attacker);
    }

    @Override
    public void apply(List<Entity> potentialTargets) {
        List<Entity> affectedTargets = this.determineAffectedTargetsThisFrame(potentialTargets);

        for (Entity target : affectedTargets) {
//            // for now, move player to origin
//            target.setPos(new Vector2D(0,100));
            target.affectByEvent(this);
        }
    }
}
