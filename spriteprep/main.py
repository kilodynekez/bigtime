from decimal import InvalidOperation
import json
import re
import shutil
import sys
import os
from typing import Any
import PIL.Image
from PIL.Image import Image

ssaDest = "../src/main/resources/sprites"


frameKeyPattern = r"(?P<animation>.*)-\((?P<layer>.*)\)-(?P<idx>\d*)"

META_REGISTRATION = "Registration"
META_HITBOX = "Hitbox"
META_HURTBOX = "Hurtbox"

metaLayerNames = [META_REGISTRATION, META_HITBOX, META_HURTBOX]

def isLeafLayer(ldata):
    return "opacity" in ldata
def isGraphicLayer(ldata):
    return (ldata["name"] not in metaLayerNames) and isLeafLayer(ldata)

def removeExt(filename):
    return ".".join(filename.split(".")[0:-1])

class Rect:
    def __init__(self, x, y):
        self.x = x 
        self.y = y
        self.w = 0
        self.h = 0
    
    def convertToOpenGL(self, heightOffset):
        y = heightOffset - self.y - self.h - 1
        r = Rect(self.x, y)
        r.w = self.w
        r.h = self.h
        return r

    def xr(self):
        return self.x + self.w -1
    def yb(self):
        return self.y + self.h -1

    def reprJSON(self):
        # del repr["metaLayers"]
        return self.__dict__

class Frame:
    def __init__(self):
        self.graphicLayers = {}
        self.metaLayers = {}

    def loadData(self, layername, fdata):
        layerSet = None
        if layername not in metaLayerNames:
            layerSet = self.graphicLayers
        else:
            layerSet = self.metaLayers

        layerSet[layername] = fdata

    def reprJSON(self):
        repr = self.__dict__.copy()
        # del repr["metaLayers"]
        return repr

class Animation:

    def __init__(self, ameta, sheetfile):
        self.name = ameta["name"]
        self.frameOffset = ameta["from"]
        self.frameCount = ameta["to"] - self.frameOffset + 1
        self.direction = ameta["direction"]
        self.frames : dict[int, Frame] = {}
        self.sheet = sheetfile

    def loadFrame(self, finfo, fdata):
        idx = finfo["idx"] - self.frameOffset
        if idx not in self.frames:
            self.frames[idx] = Frame()
        
        fr = self.frames[idx]
        fr.loadData(finfo["layer"], fdata)

    def reprJSON(self):
        return self.__dict__

def parseFrameKey(frameKey):
    m = re.match(frameKeyPattern, frameKey)
    if m is None:
        raise ValueError(f"Could not extract data from frameKey: {frameKey}")

    finfo = m.groupdict()  ## {animation, layer, idx}
    finfo["idx"] = int(finfo["idx"])
    return finfo

def parseAnimations(adata, animationMeta, sheetfilename) -> dict[str, Animation]:
    animations = {}

    for ameta in animationMeta:
        aname = ameta["name"]
        animations[aname] = Animation(ameta, sheetfilename)

    for f in adata["frames"].keys():
        finfo = parseFrameKey(f)
        fdata = adata["frames"][f]

        if len(finfo["animation"]) == 0:
            ## Skip frames that are not associated with an animation
            print(f"Skipping frame '{f}'")
            continue

        anim = animations[finfo["animation"]]
        print(f"loading frame data for {finfo} info {finfo['animation']} ({anim.name})")

        anim.loadFrame(finfo, fdata)

    return animations

def createSSA(jsonfilename):
    data = None
    with open(jsonfilename) as f:
        data = json.load(f)

    if data is None:
        raise FileNotFoundError(jsonfilename)




    ## Get animation metadata
    animationMeta = data["meta"]["frameTags"]

    ## Meta layers contain sprite/animation metadata, like frame registration and hitboxes
    metaLayers = [ldata for ldata in data["meta"]["layers"] if ldata["name"] in metaLayerNames]

    ## Graphics layers contain the actual graphics data
    graphicLayers = [ldata for ldata in data["meta"]["layers"] if isGraphicLayer(ldata)]

    print (f"Found graphics layers: {[l['name'] for l in graphicLayers]}")
    print (f"Found meta layers: {[l['name'] for l in metaLayers]}")

    ## Get the spritesheet file from the json data
    spritesheet = data["meta"]["image"]
    
    ## Parse animation data
    animations = parseAnimations(data, animationMeta, spritesheet)

    ## Load the spritesheet into memory
    sheet = PIL.Image.open(spritesheet)

    # fr = animations["Idle"].frames[0]
    # print( findOpenGLOrigin(sheet, fr, "Lisa"))
    # print( findRegistrationOffset(sheet, fr) )

    for aname in animations:
        anim = animations[aname]
        for idx in anim.frames:
            frame = anim.frames[idx]

            ## Determine registration
            try :
                regOffsetOpenGL = findRegistrationOffset(sheet, frame)
            except:
                raise RuntimeError(f"No registration found for frame in {aname}: {idx}")

            ## Get and set openGL origins for each graphics layer
            for lname in frame.graphicLayers:
                openglpos = findOpenGLOrigin(sheet, frame, lname)

                frame.graphicLayers[lname]["openGLOrigin"] = tupleToDict(openglpos)
                frame.graphicLayers[lname]["registrationOffset"] = tupleToDict(regOffsetOpenGL)

            ## Get hitbox info
            hitboxInfo = findRects(sheet, frame, META_HITBOX)
            if hitboxInfo is not None:
                frame.metaLayers[META_HITBOX] = hitboxInfo
            elif META_HITBOX in frame.metaLayers:
                del frame.metaLayers[META_HITBOX]

            ## Get hurtbox info
            hurtboxInfo = findRects(sheet, frame, META_HURTBOX)
            if hurtboxInfo is not None:
                frame.metaLayers[META_HURTBOX] = hurtboxInfo
            elif META_HURTBOX in frame.metaLayers:
                del frame.metaLayers[META_HURTBOX]

    sheet.close()

    ## Export animation data as .ssa (JSON)
    ## SSA - Sprite Sheet Animation
    outfilename = removeExt(jsonfilename) + ".ssa"
    outfile = open(outfilename, "w")

    json.dump(animations, outfile, cls=AnimationJSONEncoder, indent = 3)
    outfile.close()

    ## TODO: Cleanup and remove meta regions of spritesheet

    return outfilename, spritesheet
    

def findOpenGLOrigin(spritesheet: Image, frame: Frame, layer: str) -> tuple[int, int]:
    linfo = frame.graphicLayers[layer]
    pos = linfo["frame"]

    sheetheight = spritesheet.size[1]

    ## Find the openGL origin of this sprite (bottom left point) relative to the sprite sheet
    # openglpos = (pos["x"], sheetheight - (pos["y"] + pos["h"] - 1) -1)
    openglpos = (pos["x"], findOpenGLYCoord(pos["y"] + pos["h"] - 1, sheetheight))

    return openglpos

def findOpenGLYCoord(y: int, height: int):
    return height - y - 1


def findRects(spritesheet: Image, frame: Frame, layer: str) -> tuple[int, int]:
    if layer not in frame.metaLayers:
        return None

    ## Get info for the Registration layer
    linfo = frame.metaLayers[layer]
    pos = linfo["frame"]

    ## Extract sprite from sheet
    sprite = getSprite(spritesheet, pos["x"], pos["y"], pos["w"], pos["h"])

    ## Get alpha band
    data = list(sprite.getdata(band=3))
    (sprw, sprh) = sprite.size

    activeRects: dict[Any, Rect] = {}
    settledRects: list[Rect] = []


    ## Scan top-to-bottom, left-to-right, looking for vertical slices
    for x in range(sprw):
        slices = []
        inSlice = False
        curSliceStart = 0
        curSliceEnd = 0
        for y in range(sprh):
            val = data[x + y*sprw] > 0
            if val:  ## This pixel is solid
                if not inSlice:
                    ## If we found a pixel and we're not in a slice,
                    ## start a new slice at this y position
                    inSlice = True
                    curSliceStart = y
                    curSliceEnd = y
                else:
                    ## If we found a pixel and we're already in a slice,
                    ## then extend the current slice
                    curSliceEnd = y
            else:
                ## We didn't find a solid pixel
                if inSlice:
                    ## If we were processing a slice, then the current slice 
                    ## should now be completed and added to the slice list
                    inSlice = False
                    slice = f"{curSliceStart}:{curSliceEnd}"
                    slices.append(slice)
                    print(f"Found slice {slice},  x={x}")

                ## (If we weren't processing a slice, then do nothing)
        
        ## After we've found all the slices in this column, then create or
        ## extend all the rectangles for them
        for slice in slices:
            if slice not in activeRects:
                ## If we don't have an rectangle that corresponds to this vertical
                ## slice, then create one
                # activeRects[slice] = {
                #     "x": x,
                #     "y": slice[0],
                #     "yb": slice[1],  ## y bottom
                #     "xr": x          ## x right
                # }
                yrange = [int(p) for p in slice.split(":")]
                r = Rect(x, yrange[0])
                r.h = yrange[1] - yrange[0] + 1
                r.w = 1
                activeRects[slice] = r
                print (f"Making new slice for {slice}")
            else:
                ## If we already have an active rectangle in this slice, then extend
                ## it to this x value
                activeRects[slice].w += 1
                print (f"Widening slice for {slice}")

        ## For each active rectangle, see if we've found a new slice for it in this
        ## column.  If not, then call this rectangle complete
        toDelete = []
        for sliceKey in activeRects.keys():
            if sliceKey not in slices:
                print(f"Moving slice to settled list")
                settledRects.append(activeRects[sliceKey])
                toDelete.append(sliceKey)
        
        for sliceKey in toDelete:
            del activeRects[sliceKey]

    ## If there are any remaining active slices at the end, finalize them
    toDelete = []
    for sliceKey in activeRects.keys():
        settledRects.append(activeRects[sliceKey])

    ## If no rects were found, return Non
    if len(settledRects) == 0:
        return None


    ## Create a bounding rectangle that surrounds all the found rectangles
    boundingRect = Rect(None, None)
    for rect in settledRects:
        if boundingRect.x is None or boundingRect.x > rect.x:
            boundingRect.x = rect.x
        if boundingRect.y is None or boundingRect.y > rect.y:
            boundingRect.y = rect.y

        if boundingRect.xr() < rect.xr():
            boundingRect.w = rect.xr() - boundingRect.x +1
        if boundingRect.yb() < rect.yb():
            boundingRect.h = rect.yb() - boundingRect.y +1

    ## Get sort orders
    orderLR = findOrderLR(settledRects)
    orderRL = findOrderRL(settledRects)

    ## Convert everything to openGL format
    rects = [r.convertToOpenGL(sprh) for r in settledRects]
    boundingRect = boundingRect.convertToOpenGL(sprh)

    return {
        "rects": rects,
        "bounds": boundingRect,
        "orderLR": orderLR,
        "orderRL": orderRL
    }

def findOrderLR(rects):
    indices = range(len(rects))
    return sorted(indices, key=lambda i: rects[i].x)
def findOrderRL(rects):
    indices = range(len(rects))
    return sorted(indices, key=lambda i: rects[i].xr(), reverse=True)





def findRegistrationOffset(spritesheet: Image, frame: Frame) -> tuple[int, int]:
    ## Get info for the Registration layer
    linfo = frame.metaLayers[META_REGISTRATION]
    pos = linfo["frame"]

    sprite = getSprite(spritesheet, pos["x"], pos["y"], pos["w"], pos["h"])

    data = list(sprite.getdata(band=3))  ## get alpha band
    (w, h) = sprite.size

    xreg = None
    yreg = None

    ## Scan left-to-right, top-to-bottom, looking for a vertical line
    for y in range(h):
        for x in range(w):
            val = data[x + y*w] > 0
            if val:
                ## Assume this is the top of the vertical line
                xreg = x
                break
        if xreg is not None:
            break
                
    ## Scan top-to-bottom, left-to-right, looking for a horizontal line
    for x in range(w):
        for y in range(h):
            val = data[x + y*w] > 0
            if val:
                ## Assume this is the left side of the horizontal line
                yreg = y
                break
        if yreg is not None:
            break

    if xreg is None or yreg is None:
        raise RuntimeError("Could not find registration")

    ## Double check that the found intersection is marked in the layer
    if not data[xreg + yreg*w] > 0:
        raise InvalidOperation(f"Did not find expected intersection point at {xreg}, {yreg}")

    ## Create registration point
    reg = (xreg, yreg) 

    ## Find offset vector from bottom left point of the sprite (OpenGL origin)
    # offset = (xreg, h-yreg-1)
    offset = (xreg, findOpenGLYCoord(yreg, h))

    return offset 


def tupleToDict(tup: tuple[int, int]) -> dict[str, int]:
    return {
        "x": tup[0],
        "y": tup[1]
    }

                
def coord(x, y, data, w):
    return data[x + y*w]


def getSprite(spritesheet: Image, x: int, y: int, w: int, h: int) -> Image:
    """Gets a portion of the given sprite sheet.  Note that x and y are indexed from the top left point"""

    return spritesheet.crop((x, y, x+w, y+h))

class AnimationJSONEncoder(json.JSONEncoder):
    def __init__(self, *args, **kwargs):
        json.JSONEncoder.__init__(self, *args, **kwargs)

    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)

def main():
    for f in os.scandir():
        if f.name.endswith(".json"):
            ## Get the name of the aesprite json file
            jsonfilename = f.name

            ## Create SSA 
            print (f"Creating SSA for {f}")
            ssaFilename, spritefilename = createSSA(jsonfilename)

            ## Move results to the ssaDestination
            shutil.copy(spritefilename, os.path.join(ssaDest, spritefilename))
            shutil.copy(ssaFilename, os.path.join(ssaDest, ssaFilename))

            

if __name__ == "__main__":
    main()